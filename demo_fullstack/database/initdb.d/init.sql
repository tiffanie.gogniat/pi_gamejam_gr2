CREATE TABLE contest(
  ID  SERIAL PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  description varchar(255) not null
);

insert into contest (title, description) values ('Contest1', 'Gamejam');
insert into contest (title, description) values ('Contest2', 'Gamejam2');
insert into contest (title, description) values ('Contest3', 'Gamejam3');
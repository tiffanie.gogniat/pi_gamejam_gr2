package ch.heiafr.backend.repository;


import ch.heiafr.backend.model.Contest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContestRepository extends JpaRepository<Contest, Integer> {

    List<Contest> findByName(String name);

}

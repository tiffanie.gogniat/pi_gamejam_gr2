package ch.heiafr.backend.controller;

import ch.heiafr.backend.model.Contest;
import ch.heiafr.backend.repository.ContestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ContestCtrl {

    @Autowired
    ContestRepository contestRepository;

    @GetMapping("/contests")
    public List<Contest> getAllTutorials(@RequestParam(required = false) String title) {
        return contestRepository.findAll();
    }

    @GetMapping("/test")
    public String test(@RequestParam(required = false) String title) {
        return "route works !";
    }

    @PostMapping("/addContest")
    public List<Contest> addContest(@RequestBody Contest contest) {
		contestRepository.save(contest);
        return contestRepository.findAll();
    }

}

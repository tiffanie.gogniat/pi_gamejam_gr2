import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContestManagerComponent } from './contest-manager.component';

describe('ContestManagerComponent', () => {
  let component: ContestManagerComponent;
  let fixture: ComponentFixture<ContestManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContestManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContestManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

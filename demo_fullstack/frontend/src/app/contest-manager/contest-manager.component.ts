import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {FormControl, FormGroup, Validators} from '@angular/forms';

import {Contest} from "../model/contest";

@Component({
  selector: 'app-contest-manager',
  templateUrl: './contest-manager.component.html',
  styleUrls: ['./contest-manager.component.css']
})
export class ContestManagerComponent implements OnInit {


  public contestForm!: FormGroup;

  public contests: Contest[] = [];
  public contest: Contest = new Contest(-1, "name", "desc");

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.createContestForm();
    this.http.get<any>('http://localhost:8080/api/contests').subscribe(data => {
      console.log(data);
      this.contests = data;
    })
  }

  addContest() {
    if (this.contestForm.valid) {
      this.contest.name = this.contestForm.get('name')?.value;
      this.contest.description = this.contestForm.get('description')?.value;

      console.log(this.contest);

      this.http.post<any>('http://localhost:8080/api/addContest', this.contest).subscribe(data => {
        this.contests = data;
      })
    }
  }

  // create option form and his validators
  public createContestForm() {
    this.contestForm = new FormGroup({
      name: new FormControl('', [
        Validators.required
      ]),
      description: new FormControl('', [
        Validators.required
      ])
    })
  }

  // getters for contestForm value
  get name() {
    return this.contestForm.get('name');
  }

  get description() {
    return this.contestForm.get('description');
  }


}

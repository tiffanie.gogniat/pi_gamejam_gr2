export class Contest {
  public id: number;
  public name: string;
  public description: string;

  constructor(
    id?: number,
    name?: string,
    description?: string
  ) {
    this.id = id || -1;
    this.name = name || '';
    this.description = description || '';
  }
}

package ch.heiafr.backend.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;

import ch.heiafr.backend.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ch.heiafr.backend.service.PersonService;

/**
 * Controller REST for all persons related requests
 *
 * @author Tiffanie Gogniat
 * @version 1.1
 * @since 02.05.2022
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PersonCtrl {

    @Autowired
    PersonService personService;

    /**
     * Tries to connect the user with his credentials.
     *
     * @param bodyStr A JSON containing the mail and the password sent
     * @return A boolean confirming the successfull connexion
     */
    @PostMapping("/connect")
    public String connect(@RequestBody String bodyStr) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);
        String email = (String) body.get("email");
        String pwd = (String) body.get("pwd");

        boolean exist = personService.connect(email, pwd);
        if (exist) {
            String token = generateToken();
            Utility.addUser(email, token);
            return token;
        }
        return "";
    }

    /**
     * Disconnect a user.
     *
     * @param mail The mail of the user disconnecting
     * @return
     */
    @PostMapping("/disconnect")
    public boolean disconnect(@RequestHeader String email, @RequestHeader String token) {
        return Utility.deleteUser(email, token);
    }


    @GetMapping("/getRight")
    public String getRight(@RequestHeader String email, @RequestHeader String token) {
        if (Utility.isConnected(email, token)) {
            if (personService.isManager(email)) {
                return "MANAGER";
            } else if (personService.isJury(email)) {
                return "JURY";
            } else if (personService.isLeader(email)) {
                return "LEADER";
            } else if (personService.isParticipant(email)) {
                return "PARTICIPANT";
            } else {
                return "ERROR";
            }
        }
        return "NONE";
    }

    @GetMapping("/getUsersToken")
    public HashMap<String, String> getIntegrationValidated() {
        return Utility.usersBuffer;
    }

    private String generateToken() {
        int size = 20;
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder token = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            int index = (int) (chars.length() * Math.random());
            token.append(chars.charAt(index));
        }

        return token.toString();
    }
}

package ch.heiafr.backend.service;

import ch.heiafr.backend.repository.JuryRepository;
import ch.heiafr.backend.repository.ManagerRepository;
import ch.heiafr.backend.repository.ParticipantRepository;
import ch.heiafr.backend.repository.PersonRepository;
import ch.heiafr.backend.model.Person;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Interface between the PersonCtrl and the PersonRepository
 * @since 28.03.2022
 * @version 1.5
 */
@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ParticipantRepository participantRepository;

    @Autowired
    ManagerRepository managerRepository;

    @Autowired
    JuryRepository juryRepository;

    @Autowired
    TeamService teamService;

    /**
     * Check if the mail and password of the user correspond to the entry in
     * the database with MD5 hashing.
     * @param mail The mail of the user
     * @param pwd The password of the user
     * @return A boolean indicating the success of the connexion
     */
    public boolean connect(String mail, String pwd) {
        Person p = personRepository.findFirstByMail(mail);
        try {
            if (p != null) {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update((pwd + mail).getBytes());
                byte[] digest = md.digest();
                String hash = DatatypeConverter.printHexBinary(digest);
                return p.getPwd().toUpperCase().equals(hash.toUpperCase());
            }else{
                return false;
            }
        } catch (NoSuchAlgorithmException e) {
            return false;
        }

    }

    public boolean isParticipant(String email){
        return participantRepository.existsById(email);
    }

    public boolean isLeader(String email){
        return participantRepository.existsById(email) && teamService.isLeader(email);
    }

    public boolean isManager(String email){
        return managerRepository.existsById(email);
    }

    public boolean isJury(String email){
        return juryRepository.existsById(email);
    }

}

package ch.heiafr.backend.service;

import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.heiafr.backend.Utility;
import ch.heiafr.backend.model.Team;
import ch.heiafr.backend.model.Vote;
import ch.heiafr.backend.repository.TeamRepository;
import ch.heiafr.backend.repository.VotesRepository;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class PDFService {

    @Autowired
    VotesRepository votesRepository;

    @Autowired
    TeamRepository teamRepository;

    public ByteArrayInputStream generatePDF() {
        List<Vote> votes = getVotesList();

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, out);
            document.open();

            Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);
            Paragraph para = new Paragraph("Results", font);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            document.add(Chunk.NEWLINE);

            PdfPTable table = new PdfPTable(5);
            // Add PDF Table Header ->
            Stream.of("Game name", "Originality", "Design", "Playability", "Mean")
                    .forEach(headerTitle -> {
                        PdfPCell header = new PdfPCell();
                        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setHorizontalAlignment(Element.ALIGN_CENTER);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(headerTitle, headFont));
                        table.addCell(header);
                    });

            for (Vote vote : votes) {

                JSONParser json = new JSONParser(vote.getNotesJson());
                LinkedHashMap<String, Object> id = null;
                try {
                    id = json.parseObject();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                assert id != null;
                double originality = Double.parseDouble(String.valueOf(id.get("originality")));
                double design = Double.parseDouble(String.valueOf(id.get("design")));
                double playability = Double.parseDouble(String.valueOf(id.get("playability")));

                PdfPCell gameNameCell = new PdfPCell(new Phrase(vote.getGameName()));
                gameNameCell.setPaddingLeft(4);
                gameNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                gameNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(gameNameCell);

                PdfPCell originalityCell = new PdfPCell(new Phrase("" + originality));
                originalityCell.setPaddingLeft(4);
                originalityCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                originalityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(originalityCell);

                PdfPCell designCell = new PdfPCell(new Phrase("" + design));
                designCell.setPaddingLeft(4);
                designCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                designCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(designCell);

                PdfPCell playabilityCell = new PdfPCell(new Phrase("" + playability));
                playabilityCell.setPaddingLeft(4);
                playabilityCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                playabilityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(playabilityCell);

                PdfPCell meanCell = new PdfPCell(new Phrase("" + Math.round((originality + design + playability) / 3 * 10.0) / 10.0));
                meanCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                meanCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                meanCell.setPaddingRight(4);
                table.addCell(meanCell);
            }
            document.add(table);

            document.close();
        } catch (DocumentException e) {
            Utility.LOGGER.log(Level.FINE, e.getMessage());
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    private List<Vote> getVotesList() {
        List<Vote> meanVotes = new ArrayList();
        List<Vote> bestTeams = new ArrayList(3);
        List<Team> teams = teamRepository.findAll();
        double[] results = new double[teams.size()];
        int j = 0;
        for (Team team : teams) {
            List<Vote> votes = votesRepository.findByGameName(team.getGame());
            double meanDesign = 0;
            double meanOriginality = 0;
            double meanPlayability = 0;
            double meanTotal = 0;
            int counter = 0;
            for (Vote vote : votes) {
                LinkedHashMap<String, Object> jsonVote = Utility.getBody(vote.getNotesJson());
                meanDesign += ((BigInteger) jsonVote.get("design")).intValue();
                meanOriginality += ((BigInteger) jsonVote.get("originality")).intValue();
                meanPlayability += ((BigInteger) jsonVote.get("playability")).intValue();
                counter++;
            }
            meanDesign /= counter;
            meanOriginality /= counter;
            meanPlayability /= counter;
            meanTotal = (meanDesign + meanOriginality + meanPlayability) / 3;
            if (counter > 0) {
                results[j] = meanTotal;
            }
            JSONObject jo = new JSONObject();
            jo.put("design", Math.round(meanDesign * 10.0) / 10.0);
            jo.put("originality", Math.round(meanOriginality * 10.0) / 10.0);
            jo.put("playability", Math.round(meanPlayability * 10.0) / 10.0);
            jo.put("total", Math.round(meanTotal * 10.0) / 10.0);
            Vote meanVote = new Vote();
            meanVote.setGameName(team.getGame());
            meanVote.setNotesJson(jo.toString());
            meanVotes.add(meanVote);
            j++;
        }
        for (int i = 0; i < 3; i++) {
            int index = indexBiggestScore(results);
            if (index != -1) {
                bestTeams.add(meanVotes.get(index));
            }
        }

        return bestTeams;
    }

    private int indexBiggestScore(double[] scores) {
        double max = Double.MIN_VALUE;
        int index = 0;
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] > max) {
                max = scores[i];
                index = i;
            }
        }
        if (max != Double.MIN_VALUE) {
            scores[index] = Double.MIN_VALUE;
            return index;
        }
        return -1;
    }

}

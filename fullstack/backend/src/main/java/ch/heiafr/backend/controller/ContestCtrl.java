package ch.heiafr.backend.controller;

import ch.heiafr.backend.Utility;
import ch.heiafr.backend.model.*;
import ch.heiafr.backend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.LinkedHashMap;

/**
 * Controller REST for all contest related requests
 *
 * @version 2.0
 * @since 28.03.2022
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ContestCtrl {

    @Autowired
    ContestService contestService;

    @Autowired
    PDFService pdfService;

    @Autowired
    PersonService personService;

    /**
     * Get the current contest from the database.
     *
     * @return The contest found
     */
    @GetMapping("/getContest")
    public Contest getContest() {
        return contestService.getActiveContest();
    }

    /**
     * Generate a PDF with the results of the contest
     *
     * @return The PDF
     */
    @GetMapping("/generatePDF")
    public ResponseEntity<InputStreamResource> generatePDF() {
        ByteArrayInputStream bis = pdfService.generatePDF();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=results.pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @PostMapping("/updateContest")
    public boolean updateContest(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        System.out.println(bodyStr);
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isManager(email)) {
            LinkedHashMap<String, Object> contest = (LinkedHashMap<String, Object>) body.get("contest");

            String contestName = (String) contest.get("_contestName");
            String theme = (String) contest.get("_theme");
            String descriptionXml = (String) contest.get("_descriptionXml");
            Timestamp beginRegister = (Timestamp
                    .valueOf(((String) contest.get("_beginRegister") + ":00").replace("T", " ")));
            Timestamp endRegister = (Timestamp
                    .valueOf(((String) contest.get("_endRegister") + ":00").replace("T", " ")));
            Timestamp beginContest = (Timestamp
                    .valueOf(((String) contest.get("_beginContest") + ":00").replace("T", " ")));
            Timestamp endContest = (Timestamp.valueOf(((String) contest.get("_endContest") + ":00").replace("T", " ")));
            int counter = ((BigInteger) contest.get("_counter")).intValue();
            Contest new_contest = new Contest();
            new_contest.setContestName(contestName);
            new_contest.setTheme(theme);
            new_contest.setDescriptionXml(descriptionXml);
            new_contest.setBeginRegister(beginRegister);
            new_contest.setEndRegister(endRegister);
            new_contest.setBeginContest(beginContest);
            new_contest.setEndContest(endContest);
            new_contest.setCounter(counter);
            return contestService.updateContest(new_contest);
        }
        return false;
    }

}

package ch.heiafr.backend.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(VotePK.class)
public class Vote {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "gameName", nullable = false, length = 50)
    private String gameName;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "jury", nullable = false, length = 50)
    private String jury;
    @Basic
    @Column(name = "notesJson", nullable = true, length = -1)
    private String notesJson;

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getJury() {
        return jury;
    }

    public void setJury(String jury) {
        this.jury = jury;
    }

    public String getNotesJson() {
        return notesJson;
    }

    public void setNotesJson(String notesJson) {
        this.notesJson = notesJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return Objects.equals(gameName, vote.gameName) && Objects.equals(jury, vote.jury) && Objects.equals(notesJson, vote.notesJson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameName, jury, notesJson);
    }
}

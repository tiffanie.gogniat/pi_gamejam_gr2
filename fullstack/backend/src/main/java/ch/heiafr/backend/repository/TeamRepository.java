package ch.heiafr.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import ch.heiafr.backend.model.Team;

/**
 * The repository having access to the database, to the table Team
 */
public interface TeamRepository extends JpaRepository<Team, Integer> {

    List<Team> findAll();

    Team findTeamByLeader(String leader);

    Team findTeamByTeamName(String teamName);

    boolean existsByLeader(String leader);

    @Modifying
    @Transactional
    @Query(value = "update team set game = ?1 where teamName = ?2", nativeQuery = true)
    void setGameById(String game, String id);
    
    @Modifying
    @Transactional
    @Query(value = "update team set validated = (cast(?2 as boolean)) where teamName = cast(?1 as text)", nativeQuery = true)
    void setValidatedById(String teamName, boolean validated);

    @Query(value = "select * from team where teamname not in (select teamname from teamcomposition where participant = cast(?1 as text))", nativeQuery = true)
    List<Team> getAvailableTeams(String email);

    boolean existsByTeamNameAndValidated(String teamName, boolean validated);

    Team getByLeader(String leader);
}

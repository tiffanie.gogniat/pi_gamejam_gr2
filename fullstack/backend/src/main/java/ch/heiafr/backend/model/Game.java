package ch.heiafr.backend.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Game {
    @Id
    @Column(name = "gameName", nullable = false, length = 50)
    private String gameName;
    @Basic
    @Column(name = "filepath", nullable = true, length = 50)
    private String filepath;

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(gameName, game.gameName) && Objects.equals(filepath, game.filepath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameName, filepath);
    }
}

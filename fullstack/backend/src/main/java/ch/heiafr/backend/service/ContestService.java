package ch.heiafr.backend.service;

import ch.heiafr.backend.Utility;
import ch.heiafr.backend.model.Contest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.transaction.Transactional;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;

import ch.heiafr.backend.repository.ContestRepository;

import org.springframework.stereotype.Service;

/**
 * Interface between the ContestCtrl and the ContestRepository
 *
 * @version 1.3
 * @since 03.04.2022
 */
@Service
public class ContestService {

    @Autowired
    ContestRepository contestRepository;

    /**
     * Get the current contest
     *
     * @return The contest
     */
    public Contest getActiveContest() {
        List<Contest> list = contestRepository.findAll();
        if (list.size() > 0) {
            return contestRepository.findAll().get(0);
        }
        return null;
    }

    @Transactional
    public boolean updateContest(Contest contest) {
        String descriptionXml = contest.getDescriptionXml().replace("\\", "");
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File("schema.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new ByteArrayInputStream(descriptionXml.getBytes())));
        } catch (Exception e) {
            Utility.LOGGER.log(Level.FINE, "XML not well formed");
            return false;
        }

        if (contestRepository.count() == 0) {
            contestRepository.createContest(contest.getContestName(),
                    descriptionXml, contest.getTheme(),
                    contest.getBeginRegister(), contest.getEndRegister(),
                    contest.getBeginContest(), contest.getEndContest());
        } else {
            if (contest.getCounter() != getActiveContest().getCounter()) {
                return false;
            }
            try {
                contestRepository.updateContest(contest.getContestName(),
                        descriptionXml, contest.getTheme(),
                        contest.getBeginRegister(), contest.getEndRegister(),
                        contest.getBeginContest(), contest.getEndContest());
            } catch (ConstraintViolationException e) {
                Utility.LOGGER.log(Level.FINE, ((SQLException) e.getCause()).getMessage());
            }
        }
        return true;
    }
}

package ch.heiafr.backend.repository;


import ch.heiafr.backend.model.Contest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

/**
 * The repository having access to the database, to the table Contest
 */
public interface ContestRepository extends JpaRepository<Contest, Integer> {

    List<Contest> findByContestName(String name);

    @Modifying
    @Transactional
    @Query(value = "update contest set contestName = cast(?1 as text), descriptionXML = XMLPARSE(CONTENT ?2), theme = cast(?3 as text), "+
    "beginRegister = cast(?4 as timestamp), endRegister = cast(?5 as timestamp), beginContest = cast(?6 as timestamp), endContest = cast(?7 as timestamp)", nativeQuery = true)
    void updateContest(String name, String xml, String theme, Timestamp r_begin, Timestamp r_end, Timestamp c_begin, Timestamp c_end);


    @Modifying
    @Transactional
    @Query(value = "insert into contest (contestName, descriptionXML, theme, beginRegister, endRegister, beginContest, endContest) "+
    "values (cast(?1 as text), XMLPARSE(CONTENT ?2), cast(?3 as text), cast(?4 as timestamp), cast(?5 as timestamp), cast(?6 as timestamp), cast(?7 as timestamp))", nativeQuery = true)
    void createContest(String name, String xml, String theme, Timestamp r_begin, Timestamp r_end, Timestamp c_begin, Timestamp c_end);
}

package ch.heiafr.backend.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(TeamcompositionPK.class)
public class Teamcomposition {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "teamName", nullable = false, length = 50)
    private String teamName;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "participant", nullable = false, length = 50)
    private String participant;
    @Basic
    @Column(name = "validatedParticipant", nullable = true)
    private Boolean validatedParticipant;

    @OneToOne(targetEntity = Participant.class)
    @JoinColumn(name="participant", referencedColumnName = "person", insertable = false, updatable = false)
    private Participant participantEntity;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public Boolean getValidatedParticipant() {
        return validatedParticipant;
    }

    public void setValidatedParticipant(Boolean validated) {
        this.validatedParticipant = validated;
    }

    public Participant getParticipantEntity(){
        return participantEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teamcomposition that = (Teamcomposition) o;
        return Objects.equals(teamName, that.teamName) && Objects.equals(participant, that.participant) && Objects.equals(validatedParticipant, that.validatedParticipant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamName, participant, validatedParticipant);
    }
}

package ch.heiafr.backend;

import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Logger;

public class Utility {

    public static final Logger LOGGER = Logger.getLogger("backend");

    public static HashMap<String, String> usersBuffer = new HashMap<>();

    public static boolean isConnected(String email, String token) {
        if (token != null && email != null && token.equals(usersBuffer.get(email))) {
            return true;
        }
        return false;
    }

    public static void addUser(String email, String token) {
        usersBuffer.put(email, token);
    }

    public static boolean deleteUser(String email, String token) {
        if (token.equals(usersBuffer.get(email))) {
            usersBuffer.remove(email);
            return true;
        }
        return false;
    }


    public static LinkedHashMap<String, Object> getBody(String body) {
        JSONParser json = new JSONParser(body);
        LinkedHashMap<String, Object> infos = null;
        try {
            infos = json.parseObject();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return infos;
    }
}

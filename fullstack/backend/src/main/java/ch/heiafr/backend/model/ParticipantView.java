package ch.heiafr.backend.model;


import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Immutable
public class ParticipantView {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "mail", nullable = false, length = 50)
    private String mail;

    @Column(name = "firstname", nullable = false, length = 50)
    private String firstname;

    @Column(name = "lastname", nullable = false, length = 50)
    private String lastname;

    @Basic
    @Column(name = "birthdate", nullable = false)
    private Date birthdate;
    @Basic
    @Column(name = "addr", nullable = true, length = 50)
    private String addr;
    @Basic
    @Column(name = "zip", nullable = true)
    private Integer zip;
    @Basic
    @Column(name = "locality", nullable = true, length = 50)
    private String locality;

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMail() {
        return mail;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getAddr() {
        return addr;
    }

    public Integer getZip() {
        return zip;
    }

    public String getLocality() {
        return locality;
    }


}

package ch.heiafr.backend.repository;

import ch.heiafr.backend.model.Participant;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ParticipantRepository extends JpaRepository<Participant, String> {

}

package ch.heiafr.backend.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Participant {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "person", nullable = false, length = 50)
    private String person;
    @Basic
    @Column(name = "birthdate", nullable = false)
    private Date birthdate;
    @Basic
    @Column(name = "addr", nullable = true, length = 50)
    private String addr;
    @Basic
    @Column(name = "zip", nullable = true)
    private Integer zip;
    @Basic
    @Column(name = "locality", nullable = true, length = 50)
    private String locality;

    @OneToOne(targetEntity = Person.class)
    @JoinColumn(name="person", referencedColumnName = "mail", insertable = false, updatable = false)
    private Person personEntity;

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public Person getPersonEntity(){
        return personEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participant that = (Participant) o;
        return Objects.equals(person, that.person) && Objects.equals(birthdate, that.birthdate) && Objects.equals(addr, that.addr) && Objects.equals(zip, that.zip) && Objects.equals(locality, that.locality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, birthdate, addr, zip, locality);
    }
}

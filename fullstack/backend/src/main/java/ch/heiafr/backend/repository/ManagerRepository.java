package ch.heiafr.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.heiafr.backend.model.Manager;

public interface ManagerRepository extends JpaRepository<Manager, String> {
    
}

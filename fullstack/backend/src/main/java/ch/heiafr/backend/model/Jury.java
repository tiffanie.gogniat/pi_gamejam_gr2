package ch.heiafr.backend.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Jury {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "person", nullable = false, length = 50)
    private String person;

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jury jury = (Jury) o;
        return Objects.equals(person, jury.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person);
    }
}

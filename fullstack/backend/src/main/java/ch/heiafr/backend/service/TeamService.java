package ch.heiafr.backend.service;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;

import ch.heiafr.backend.Utility;
import ch.heiafr.backend.model.Game;
import ch.heiafr.backend.model.Person;
import ch.heiafr.backend.model.Team;
import ch.heiafr.backend.model.Teamcomposition;
import ch.heiafr.backend.model.VotePK;
import ch.heiafr.backend.repository.GameRepository;
import ch.heiafr.backend.repository.TeamCompositionRepository;
import ch.heiafr.backend.repository.TeamRepository;
import ch.heiafr.backend.repository.VotesRepository;

import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Interface between the TeamCtrl and the TeamRepository
 * 
 * @since 28.03.2022
 * @version 1.12
 */
@Service
public class TeamService {

    private static final String BASE_PATH = "games/";

    @Autowired
    TeamCompositionRepository teamCompositionRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    VotesRepository votesRepository;

    /**
     * Get the list of registered teams
     * 
     * @return A list of teams
     */
    public List<Team> getTeamsInscriptions(String email) {
        if (!teamCompositionRepository.existsByParticipantAndValidatedParticipant(email, true)) {
            return teamRepository.getAvailableTeams(email);
        }
        return new ArrayList<Team>();
    }

    /**
     * Retrieve the integration request to the specific team
     * 
     * @param team_name The name of the team
     * @return A list of integration requests
     */
    public JSONArray getIntegrationRequests(String team_name) {
        List<Teamcomposition> list = teamCompositionRepository
                .findTeamcompositionByValidatedParticipantAndTeamName(null, team_name);
        JSONArray ja = new JSONArray();
        for (Teamcomposition req : list) {
            JSONObject jo = new JSONObject();
            jo.put("participant", req.getParticipant());
            jo.put("lastname", req.getParticipantEntity().getPersonEntity().getLastname());
            jo.put("firstname", req.getParticipantEntity().getPersonEntity().getFirstname());
            ja.put(jo);
        }

        return ja;
    }

    /**
     * Get the user's team name
     * 
     * @param mail The mail of the user
     * @return The name of the team
     */
    public String getTeamName(String mail) {
        if (!teamRepository.existsByLeader(mail)) {
            return "";
        }
        return teamRepository.findTeamByLeader(mail).getTeamName();
    }

    /**
     * Create a new team
     * 
     * @param team The team to create
     */
    public void createTeam(Team team) {
        try {
            teamRepository.save(team);
            teamCompositionRepository.deleteIntegrationRequests(team.getLeader());
        } catch (ConstraintViolationException e) {
            Utility.LOGGER.log(Level.FINE, ((SQLException) e.getCause()).getMessage());
        }
    }

    /**
     * Get the validated integration requests of a team
     * 
     * @param teamName The name of the team
     * @return A list of integration requests
     */
    public List<Teamcomposition> getIntegrationValidated(String teamName) {
        return teamCompositionRepository.findTeamcompositionByValidatedParticipantAndTeamName(true, teamName);
    }

    /**
     * Create an integration request for a participant and a team
     * 
     * @param teamName    The name of the team
     * @param participant The mail of the participant
     */
    public void requestIntegration(String teamName, String participant) {
        teamCompositionRepository.requestIntegration(teamName, participant);
    }

    /**
     * Copy the file containing the game to the server and add the path in the
     * database
     * 
     * @param file The file containing the game
     * @param team The name of the team
     * @throws IOException
     */
    public void uploadGame(MultipartFile file, String team) throws IOException {
        Path filePath = Paths.get(BASE_PATH + team + "-" + file.getOriginalFilename());

        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        teamRepository.setGameById(filePath.toString(), team);
    }

    @Transactional
    public boolean uploadGame(String name, String email) {
        Team team = teamRepository.getByLeader(email);
        if (gameRepository.existsById(name) || teamRepository.getByLeader(email).getGame() != null || !teamRepository.existsByTeamNameAndValidated(team.getTeamName(), true)) {
            return false;
        }
        Game game = new Game();
        game.setGameName(name);
        gameRepository.save(game);
        teamRepository.setGameById(name, team.getTeamName());
        return true;
    }

    /**
     * Accept an integration request
     * 
     * @param team        The name of the team
     * @param participant The mail of the participant we want to accept
     */
    public void acceptIntegrationRequest(String team, String participant) {
        try {
            teamCompositionRepository.setValidatedById(Boolean.parseBoolean("true"), team, participant);
            teamCompositionRepository.deleteIntegrationRequests(participant);
        } catch (ConstraintViolationException e) {
            Utility.LOGGER.log(Level.FINE, ((SQLException) e.getCause()).getMessage());
        }
    }

    /**
     * Refuse an integration request
     * 
     * @param team        The name of the team
     * @param participant The mail of the participant we want to refuse
     */
    public void refuseIntegrationRequest(String team, String participant) {
        teamCompositionRepository.setValidatedById(Boolean.parseBoolean("false"), team, participant);
    }

    /**
     * Check if the given user is a team leader
     * 
     * @param email The mail of the user
     */
    public boolean isLeader(String email) {
        return teamRepository.existsByLeader(email);
    }

    /**
     * Get the list of the persons in a specific team
     * 
     * @param team_name The name of the team
     * @return The list of participant and the leader of the team
     */
    public List<Person> getTeamParticipants(String team_name) {
        List<Teamcomposition> teamcompositions = teamCompositionRepository
                .findTeamcompositionByValidatedParticipantAndTeamName(Boolean.parseBoolean("true"), team_name);
        List<Person> persons = new ArrayList<Person>();
        Team t = teamRepository.findTeamByTeamName(team_name);
        Person p = t.getLeaderEntity().getPersonEntity();
        p.setPwd("");
        persons.add(p);
        for (Teamcomposition teamc : teamcompositions) {
            p = teamc.getParticipantEntity().getPersonEntity();
            p.setPwd("");
            persons.add(p);
        }
        return persons;
    }

    /**
     * Validate a team
     * 
     * @param teamName The name of the team to validate
     */
    public void acceptTeam(String teamName) {
        teamRepository.setValidatedById(teamName, true);
    }

    /**
     * Invalidate a team
     * 
     * @param teamName The name of the team to invalidate
     */
    public void refuseTeam(String teamName) {
        teamRepository.setValidatedById(teamName, false);
    }

    public List<Game> getGames() {
        return gameRepository.findAll();
    }

    public void sendVotes(ArrayList<LinkedHashMap<String, Object>> votes) {
        for (LinkedHashMap<String, Object> vote : votes) {
            VotePK votePK = new VotePK();
            votePK.setGameName((String) vote.get("_game"));
            votePK.setJury((String) vote.get("_jury"));
            JSONObject jo = new JSONObject();
            LinkedHashMap<String, Object> v = (LinkedHashMap<String, Object>) vote.get("_notesJSON");
            jo.put("originality", ((BigInteger) v.get("originality")).intValue());
            jo.put("design", ((BigInteger) v.get("design")).intValue());
            jo.put("playability", ((BigInteger) v.get("playability")).intValue());

            try {
                if (votesRepository.existsById(votePK)) {
                    System.out.println(vote.get("_notesJSON").toString());
                    votesRepository.updateVote((String) vote.get("_game"), (String) vote.get("_jury"),
                            jo.toString());
                } else {
                    System.out.println(vote.get("_notesJSON").toString());
                    votesRepository.saveVote((String) vote.get("_game"), (String) vote.get("_jury"),
                            jo.toString());
                }
            } catch (ConstraintViolationException e) {
                Utility.LOGGER.log(Level.FINE, ((SQLException) e.getCause()).getMessage());
            }
        }
    }
}

package ch.heiafr.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.heiafr.backend.model.Jury;

public interface JuryRepository extends JpaRepository<Jury, String> {
    
}

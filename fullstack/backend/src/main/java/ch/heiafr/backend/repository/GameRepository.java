package ch.heiafr.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.heiafr.backend.model.Game;

public interface GameRepository extends JpaRepository<Game, String> {
    
}

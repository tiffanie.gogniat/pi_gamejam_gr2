package ch.heiafr.backend.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Team {
    @Id
    @Column(name = "teamName", nullable = false, length = 50)
    private String teamName;
    @Basic
    @Column(name = "leader", nullable = false, length = 50)
    private String leader;
    @Basic
    @Column(name = "contest", nullable = false, length = 50)
    private String contest;
    @Basic
    @Column(name = "game", nullable = true, length = 50)
    private String game;
    @Basic
    @Column(name = "validated")
    private Boolean validated;
    @Basic
    @Column(name = "requestValidation", nullable = true, length = 50)
    private Boolean requestValidation;

    @OneToOne(targetEntity = Participant.class)
    @JoinColumn(name="leader", referencedColumnName = "person", insertable = false, updatable = false)
    private Participant leaderEntity;

    public Team() {

    }

    public String getTeamName() {
        return teamName;
    }

    public Team setTeamName(String teamName) {
        this.teamName = teamName;
        return this;
    }

    public String getLeader() {
        return leader;
    }

    public Team setLeader(String leader) {
        this.leader = leader;
        return this;
    }

    public String getContest() {
        return contest;
    }

    public Team setContest(String contest) {
        this.contest = contest;
        return this;
    }

    public String getGame() {
        return game;
    }

    public Team setGame(String game) {
        if (Objects.equals(game, ""))
            this.game = null;
        else
            this.game = game;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(teamName, team.teamName) && Objects.equals(leader, team.leader) && Objects.equals(contest, team.contest) && Objects.equals(game, team.game);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamName, leader, contest, game);
    }

    public Boolean getValidated() {
        return validated;
    }

    public Team setValidated(Boolean validated) {
        this.validated = validated;
        return this;
    }

    public Boolean getRequestValidation() {
        return requestValidation;
    }

    public Team setRequestValidation(Boolean requestValidation) {
        this.requestValidation = requestValidation;
        return this;
    }

    public Participant getLeaderEntity(){
        return leaderEntity;
    }
}

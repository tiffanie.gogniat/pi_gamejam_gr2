package ch.heiafr.backend.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(ContestmanagementPK.class)
public class Contestmanagement {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "contestName", nullable = false, length = 50)
    private String contestName;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "manager", nullable = false, length = 50)
    private String manager;

    public String getContestName() {
        return contestName;
    }

    public void setContestName(String contestName) {
        this.contestName = contestName;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contestmanagement that = (Contestmanagement) o;
        return Objects.equals(contestName, that.contestName) && Objects.equals(manager, that.manager);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestName, manager);
    }
}

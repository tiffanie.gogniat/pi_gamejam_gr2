package ch.heiafr.backend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import ch.heiafr.backend.model.Teamcomposition;

/**
 * The repository having access to the database, to the table Teamcomposition
 */
public interface TeamCompositionRepository extends JpaRepository<Teamcomposition, Integer> {

    List<Teamcomposition> findTeamcompositionByValidatedParticipant(boolean validated);

    List<Teamcomposition> findTeamcompositionByValidatedParticipantAndTeamName(Boolean validated, String teamName);

    @Modifying
    @Transactional
    @Query(value = "update teamComposition set validatedParticipant = (cast(?1 as boolean)) where teamName = (cast(?2 as text)) and participant = (cast(?3 as text))", nativeQuery = true)
    int setValidatedById(boolean validated, String teamName, String id);

    @Modifying
    @Transactional
    @Query(value = "insert into teamComposition (teamname, participant, validatedparticipant) values (cast(?1 as text), cast(?2 as text), NULL)", nativeQuery = true)
    void requestIntegration(String teamName, String participant);

    @Modifying
    @Transactional
    @Query(value = "delete from teamComposition where participant = (cast(?1 as text)) and validatedparticipant is null", nativeQuery = true)
    void deleteIntegrationRequests(String participant);

    boolean existsByParticipantAndValidatedParticipant(String participant, boolean validated);
}

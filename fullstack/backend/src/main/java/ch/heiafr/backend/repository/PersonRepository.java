package ch.heiafr.backend.repository;

import ch.heiafr.backend.model.Person;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The repository having access to the database, to the table Person
 */
public interface PersonRepository extends JpaRepository<Person, String> {
    Person findFirstByMail(String mail);
}

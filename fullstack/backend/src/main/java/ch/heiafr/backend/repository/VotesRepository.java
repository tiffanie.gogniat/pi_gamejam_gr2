package ch.heiafr.backend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import ch.heiafr.backend.model.Vote;
import ch.heiafr.backend.model.VotePK;

public interface VotesRepository extends JpaRepository<Vote, VotePK> {
    
    @Modifying
    @Transactional
    @Query(value = "insert into vote (gameName, jury, notesJson) values (cast(?1 as text), cast(?2 as text), cast(?3 as text))", nativeQuery = true)
    void saveVote(String gameName, String jury, String notesJson);

    @Modifying
    @Transactional
    @Query(value = "update vote set notesJson = cast(?3 as text) where jury = cast(?2 as text) and gameName = cast(?1 as text)", nativeQuery = true)
    void updateVote(String gameName, String jury, String notesJson);

    List<Vote> findByGameName(String teamName);
}

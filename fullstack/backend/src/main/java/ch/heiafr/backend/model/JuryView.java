package ch.heiafr.backend.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
public class JuryView {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "mail", nullable = false, length = 50)
    private String mail;

    @Column(name = "firstname", nullable = false, length = 50)
    private String firstname;

    @Column(name = "lastname", nullable = false, length = 50)
    private String lastname;

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMail() {
        return mail;
    }

}

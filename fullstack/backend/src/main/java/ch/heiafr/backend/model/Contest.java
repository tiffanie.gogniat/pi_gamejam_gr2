package ch.heiafr.backend.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@IdClass(Contest.class)
@Table(name = "contest", schema = "public")
public class Contest implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "contestName", nullable = false, length = 50)
    private String contestName;
    @Basic
    @Column(name = "theme", nullable = true, length = 50)
    private String theme;
    @Basic
    @Column(name = "descriptionXml", nullable = true)
    private String descriptionXml;
    @Basic
    @Column(name = "beginRegister", nullable = true)
    private Timestamp beginRegister;
    @Basic
    @Column(name = "endRegister", nullable = true)
    private Timestamp endRegister;
    @Basic
    @Column(name = "beginContest", nullable = true)
    private Timestamp beginContest;
    @Basic
    @Column(name = "endContest", nullable = true)
    private Timestamp endContest;
    @Basic
    @Column(name = "counter", nullable = false)
    private int counter;

    public String getContestName() {
        return contestName;
    }

    public void setContestName(String contestName) {
        this.contestName = contestName;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDescriptionXml() {
        return descriptionXml;
    }

    public void setDescriptionXml(String descriptionXml) {
        this.descriptionXml = descriptionXml;
    }

    public Timestamp getBeginRegister() {
        return beginRegister;
    }

    public void setBeginRegister(Timestamp beginRegister) {
        this.beginRegister = beginRegister;
    }

    public Timestamp getEndRegister() {
        return endRegister;
    }

    public void setEndRegister(Timestamp endRegister) {
        this.endRegister = endRegister;
    }

    public Timestamp getBeginContest() {
        return beginContest;
    }

    public void setBeginContest(Timestamp beginContest) {
        this.beginContest = beginContest;
    }

    public Timestamp getEndContest() {
        return endContest;
    }

    public void setEndContest(Timestamp endContest) {
        this.endContest = endContest;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getCounter(){
        return this.counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return Objects.equals(contestName, contest.contestName) && Objects.equals(theme, contest.theme) && Objects.equals(descriptionXml, contest.descriptionXml) && Objects.equals(beginRegister, contest.beginRegister) && Objects.equals(endRegister, contest.endRegister) && Objects.equals(beginContest, contest.beginContest) && Objects.equals(endContest, contest.endContest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestName, theme, descriptionXml, beginRegister, endRegister, beginContest, endContest);
    }
}

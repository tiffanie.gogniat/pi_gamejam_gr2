package ch.heiafr.backend.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import ch.heiafr.backend.Utility;
import ch.heiafr.backend.model.Game;
import ch.heiafr.backend.model.Person;
import ch.heiafr.backend.model.Team;
import ch.heiafr.backend.service.PersonService;
import ch.heiafr.backend.service.TeamService;

/**
 * Controller REST for all team related requests
 *
 * @author Tiffanie Gogniat
 * @version 1.0
 * @since 02.05.2022
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class TeamCtrl {
    @Autowired
    TeamService teamService;

    @Autowired
    PersonService personService;

    /**
     * Create a new team
     *
     * @param params A JSON containing the name of the team, the mail of the leader,
     *               the contest it is related to and the name of the game
     */
    @PostMapping("/createTeam")
    public boolean createTeam(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isParticipant(email)) {
            String teamName = (String) body.get("teamName");
            String contest = (String) body.get("contest");
            String game = (String) body.get("game");
            Team team = new Team().setTeamName(teamName).setContest(contest).setLeader(email).setGame(game);
            teamService.createTeam(team);
            return true;
        }
        return false;
    }

    /**
     * Create an integration request for the user to the specified team
     *
     * @param params A JSON containing the mail of the requesting user and the
     *               name of the team the user wants to join
     */
    @PostMapping("/requestIntegration")
    public boolean requestIntegration(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isParticipant(email)) {
            String teamName = (String) body.get("teamName");
            teamService.requestIntegration(teamName, email);
            return true;
        }
        return false;
    }

    /**
     * Accept the integration request of a user in a team
     *
     * @param params A JSON containing the mail of the user to accept and the
     *               name of the team
     */
    @PostMapping("/acceptIntegrationRequest")
    public boolean acceptIntegrationRequest(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isLeader(email)) {
            String teamName = (String) body.get("teamName");
            String participant = (String) body.get("participant");
            teamService.acceptIntegrationRequest(teamName, participant);
            return true;
        }
        return false;
    }

    /**
     * Refuse the integration request of a user in a team
     *
     * @param params A JSON containing the mail of the user to accept and the
     *               name of the team
     */
    @PostMapping("/refuseIntegrationRequest")
    public boolean refuseIntegrationRequest(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isLeader(email)) {
            String teamName = (String) body.get("teamName");
            String participant = (String) body.get("participant");
            teamService.refuseIntegrationRequest(teamName, participant);
            return true;
        }
        return false;
    }

    /**
     * Upload a file containing the game of a team.
     *
     * @param file The file
     * @param team The team
     * @throws IOException
     */
    /*
     * @PostMapping("/uploadGame")
     * public void uploadGame(@RequestParam("file") MultipartFile
     * file, @RequestParam String team) throws IOException {
     * teamService.uploadGame(file, team);
     * }
     */
    @PostMapping("/uploadGame")
    public boolean uploadGame(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) throws IOException {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isLeader(email)) {
            String gameName = (String) body.get("gameName");
            return teamService.uploadGame(gameName, email);
        }
        return false;
    }

    /**
     * Get the integration request received by other users
     *
     * @param team_name The name of the team we want the integration requests for
     * @return A list of integration requests
     */
    @GetMapping("/getIntegrationRequests/{team_name}")
    public String getIntegrationRequests(@PathVariable String team_name, @RequestHeader String email, @RequestHeader String token) {
        if (Utility.isConnected(email, token) && personService.isLeader(email)) {
            return teamService.getIntegrationRequests(team_name).toString();
        }
        return "";
    }

    /**
     * Retrieve the teams which the user can join
     *
     * @return A list of team
     */
    @GetMapping("/getTeamsInscription")
    public List<Team> getTeamsInscriptions(@RequestHeader String email, @RequestHeader String token) {
        if (Utility.isConnected(email, token) && (personService.isParticipant(email) || personService.isManager(email))) {
            return teamService.getTeamsInscriptions(email);
        }
        return null;
    }

    /**
     * Get the user's team name.
     *
     * @param mail The mail of the user
     * @return The user's team name
     */
    @GetMapping("/getTeamName")
    public String getTeamName(@RequestHeader String email, @RequestHeader String token) {
        if (Utility.isConnected(email, token) && (personService.isParticipant(email) || personService.isLeader(email))) {
            return teamService.getTeamName(email);
        }
        return "";
    }

    /**
     * Get the integration requests that were validated
     *
     * @param team_name The name of the team we want the information for
     * @return A list of validated integration request
     */
    @GetMapping("/getIntegrationValidated/{team_name}")
    public long getIntegrationValidated(@PathVariable String team_name, @RequestHeader String email, @RequestHeader String token) {
        return teamService.getIntegrationValidated(team_name).size() + 1;
    }

    @GetMapping("/getTeamParticipants/{team_name}")
    public List<Person> getTeamParticipants(@PathVariable String team_name, @RequestHeader String email, @RequestHeader String token) {
        if (Utility.isConnected(email, token) && personService.isManager(email)) {
            return teamService.getTeamParticipants(team_name);
        }
        return null;
    }

    @PostMapping("/acceptTeam")
    public boolean acceptTeam(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isManager(email)) {
            String teamName = (String) body.get("teamName");
            teamService.acceptTeam(teamName);
            return true;
        }
        return false;
    }

    @PostMapping("/refuseTeam")
    public boolean refuseTeam(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isManager(email)) {
            String teamName = (String) body.get("teamName");
            teamService.refuseTeam(teamName);
            return true;
        }
        return false;
    }

    @GetMapping("/getGames")
    public List<Game> getGames(@RequestHeader String email, @RequestHeader String token) {
        if (Utility.isConnected(email, token) && personService.isJury(email)) {
            return teamService.getGames();
        }
        return null;
    }

    @PostMapping("/sendVotes")
    public boolean sendVotes(@RequestBody String bodyStr, @RequestHeader String email, @RequestHeader String token) {
        LinkedHashMap<String, Object> body = Utility.getBody(bodyStr);

        if (Utility.isConnected(email, token) && personService.isJury(email)) {
            ArrayList<LinkedHashMap<String, Object>> votes = (ArrayList<LinkedHashMap<String, Object>>) body
                    .get("votes");
            teamService.sendVotes(votes);
            return true;
        }
        return false;
    }

}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {RegistrationComponent} from "./components/registration/registration.component";
import {CreationTeamComponent} from "./components/creation-team/creation-team.component";
import {IntegrationRequestsComponent} from "./components/integration-requests/integration-requests.component";
import {HomeComponent} from "./components/home/home.component";
import {CreationContestComponent} from "./components/creation-contest/creation-contest.component";
import {AuthGuard} from "./auth.guard";
import {RatingGameComponent} from "./components/rating-game/rating-game.component";
import {TeamValidationComponent} from "./components/team-validation/team-validation.component";
import {UploadGameComponent} from "./components/upload-game/upload-game.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'disconnect', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent, canActivate: [AuthGuard]},
  {path: 'uploadGame', component: UploadGameComponent, canActivate: [AuthGuard]},
  {path: 'createTeam', component: CreationTeamComponent, canActivate: [AuthGuard]},
  {path: 'createContest', component: CreationContestComponent, canActivate: [AuthGuard]},
  {path: 'integrationRequests', component: IntegrationRequestsComponent, canActivate: [AuthGuard]},
  {path: 'ratingGame', component: RatingGameComponent, canActivate: [AuthGuard]},
  {path: 'teamValidation', component: TeamValidationComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Component, OnInit} from '@angular/core';
import {ContestService} from "../../services/contest.service";
import {Contest} from "../../models/contest";
import {XMLParser} from 'fast-xml-parser';

/**
 * HomeComponent implements view of the home page and its functions.
 *
 * @author Guillaume Etienne
 * @version 2.0
 * @since 03.04.2022
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  /** current contest */
  private currentContest: Contest | undefined;
  public contestDescription: any = [];


  /**
   * Contructor of the HomeComponent
   *
   * @param contestService Service that manages all HTTP requests for the contest.
   */
  constructor(private contestService: ContestService) {
  }

  /**
   * Called at the init of the component.
   * Uses contestService to get the current contest.
   */
  ngOnInit(): void {
    this.contestService.getContestInfos().subscribe({
      next: (contest) => {
        console.log(contest);
        if (contest != null) {
          console.log("set contest");
          this.contestService.setContest(contest);
          const options = {
            ignoreAttributes: false,
            attributeNamePrefix: ""
          };
          const parser = new XMLParser(options);
          this.contestDescription = parser.parse(contest.descriptionXml).description;
          console.log(this.contestDescription);
        } else {
          console.log("new contest");
          this.contestService.setContest(new Contest())
          this.contestDescription = null
        }
        //this.contestDescription.image = "https://i-mom.unimedias.fr/2022/03/14/chat.jpg?auto=format,compress&cs=tinysrgb";
      }
    })
  }


  /**
   * Get current contest.
   *
   * @return contest
   */
  contest() {
    if (!this.currentContest) {
      this.currentContest = this.contestService.getContest();
    }
    return this.currentContest;
  }

}

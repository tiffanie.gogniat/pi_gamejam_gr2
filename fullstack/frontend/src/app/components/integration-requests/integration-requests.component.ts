import {Component, OnInit} from '@angular/core';
import {TeamComposition} from "../../models/teamComposition";
import {Team} from "../../models/team";
import {TeamService} from "../../services/team.service";
import {PersonService} from "../../services/person.service";
import {Router} from '@angular/router';


/**
 * IntegrationRequestsComponent implements view of the integration requests and its functions.
 *
 * @author Guillaume Etienne
 * @version 2.1
 * @since 28.03.2022
 */
@Component({
  selector: 'app-integration-requests',
  templateUrl: './integration-requests.component.html',
  styleUrls: ['./integration-requests.component.css']
})
export class IntegrationRequestsComponent implements OnInit {

  /** Requests to integrate the manager's team */
  public requests: TeamComposition[] = [];

  /** Team's name of the manager */
  public teamName: string = "";


  /**
   * Constructor of the IntegrationRequestsComponent
   *
   * @param teamService Service that manages all HTTP requests for the team.
   * @param personService Service that manages all HTTP requests for persons.
   * @param router Service that manages navigation between components.
   */
  constructor(private teamService: TeamService, private personService: PersonService, private router: Router) {
  }

  /**
   * Called at the init of the component.
   * Get integration requests from teamService.
   */
  ngOnInit(): void {
    this.teamService.getTeamName().subscribe({
      next: (teamName) => {
        this.teamName = teamName;
        this.teamService.getIntegrationRequests(teamName).subscribe({
          next: (data) => {
            console.log(data);
            this.requests = data;
          },
          error: (err) => {
          }
        });
      },
      error: (err) => {
      }
    });

  }


  /**
   * Accepts integration request.
   *
   * @param email Email of accepted person
   */
  accept(email: any) {
    this.teamService.acceptRequestIntegration(email, this.teamName).subscribe({
      next: (data) => {
        if (data) {
          alert("Participant accepté !");
          window.location.reload();
        } else {
          alert("Erreur lors de l'acceptation du participant !");
        }
      },
      error: (err) => {
        alert("Erreur");
      }
    });
  }


  /**
   * Refuses integration request.
   *
   * @param email Email of accepted person
   */
  refuse(email: any) {
    this.teamService.refuseRequestIntegration(email, this.teamName).subscribe({
      next: (data) => {
        if (data) {
          alert("Participant refusé !");
          window.location.reload();
        } else {
          alert("Erreur lors du refus du participant !");
        }
      },
      error: (err) => {
        alert("Erreur");
      }
    });
  }


}

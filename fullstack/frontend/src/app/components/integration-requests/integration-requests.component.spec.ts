import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationRequestsComponent } from './integration-requests.component';

describe('IntegrationRequestsComponent', () => {
  let component: IntegrationRequestsComponent;
  let fixture: ComponentFixture<IntegrationRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntegrationRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TeamService} from "../../services/team.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-upload-game',
  templateUrl: './upload-game.component.html',
  styleUrls: ['./upload-game.component.css']
})
export class UploadGameComponent implements OnInit {

  /** Form for game creation */
  public gameForm!: FormGroup;

  constructor(private teamService: TeamService, private router: Router) {
  }

  ngOnInit(): void {
    this.createGameForm();
  }

  createGame() {
    if (this.gameForm.valid) {
      let gameName = this.gameForm.get("gameName")?.value;
      console.log(gameName);
      this.teamService.uploadGame(gameName).subscribe({
        next: (data) => {
          if (data == true) {
            alert("Votre jeu a été déposé");
            this.router.navigate([''])
          } else {
            alert("Votre jeu n'a pas pu être déposé");
          }
        },
        error: (err) => {
          alert("Votre jeu n'a pas pu être déposé");
        }
      });
    }
  }

  /**
   * Creates form for team creation.
   */
  createGameForm() {
    this.gameForm = new FormGroup({
      gameName: new FormControl('', [
        Validators.required
      ])
    });
  }


  /** Getter for team's name in form */
  get gameName() {
    return this.gameForm.get('gameName');
  }

}

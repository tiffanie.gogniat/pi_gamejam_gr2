import {Component, Input, OnInit} from '@angular/core';


/**
 * PageTitleComponent implements view of title page and its functions.
 *
 * @author Guillaume Etienne
 * @version 1.0
 * @since 28.03.2022
 */
@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.css']
})
export class PageTitleComponent implements OnInit {

  /** Contest's title, component's input */
  @Input() titleText!: string;


  /**
   * Constructor of the PageTitleComponent
   */
  constructor() {
  }


  /**
   * Called at the init of the component.
   */
  ngOnInit(): void {
  }

}

import {Component, OnInit} from '@angular/core';
import {Game} from "../../models/game";
import {Vote} from "../../models/vote";
import {PersonService} from "../../services/person.service";
import {TeamService} from "../../services/team.service";
import {Router} from "@angular/router";


/**
 * RatingGameComponent implements view of the games rating and its functions.
 *
 * @author Guillaume Etienne
 * @version 1.0
 * @since 06.05.2022
 */
@Component({
  selector: 'app-rating-game',
  templateUrl: './rating-game.component.html',
  styleUrls: ['./rating-game.component.css']
})
export class RatingGameComponent implements OnInit {

  /** All games in contest */
  public games: Game[] = [];


  /**
   * Constructor of the RatingGameComponent
   *
   * @param personService personService Service that manages all HTTP requests for persons.
   * @param teamService teamService Service that manages all HTTP requests for the team.
   * @param router Service that manages navigation between components.
   */
  constructor(private personService: PersonService, private teamService: TeamService, private router: Router) {
  }


  /**
   * Called at the init of the component.
   * Gets games from backend.
   */
  ngOnInit(): void {
    this.teamService.getGames().subscribe({
      next: (data) => {
        this.games = data;
      },
      error: (err) => {
        alert("Echec de l'envoie des votes !");

      }
    });
  }


  /**
   * Send votes from current jury.
   */
  sendVotes() {
    let votes: Vote[] = [];
    for (let game of this.games) {
      let rating = {
        "originality": game.originality - 1,
        "design": game.design - 1,
        "playability": game.playability - 1,
      }
      let vote = new Vote(this.personService.getUser(), game.gameName, rating);
      votes.push(vote);
    }

    this.teamService.sendVotes(votes).subscribe({
      next: (data) => {
        if (data == true) {
          alert("Les votes ont été envoyés !");
          this.router.navigate(['']);
        } else {
          alert("Echec de l'envoi des votes !");
        }
      },
      error: (err) => {
        alert("Echec de l'envoi des votes !");

      }
    });
  }
}

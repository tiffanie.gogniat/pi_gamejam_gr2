import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TeamService} from "../../services/team.service";
import {Router} from "@angular/router";
import {PersonService} from "../../services/person.service";

/**
 * CreationTeamComponent implements view of the team creation and its functions.
 *
 * @author Guillaume Etienne
 * @version 2.0
 * @since 28.03.2022
 */
@Component({
  selector: 'app-creation-team',
  templateUrl: './creation-team.component.html',
  styleUrls: ['./creation-team.component.css']
})
export class CreationTeamComponent implements OnInit {

  /** Form for team creation */
  public teamForm!: FormGroup;

  /**
   * Constructor of CreationTeamComponent
   *
   * @param teamService Service that manages all HTTP requests for the team.
   * @param personService Service that manages all HTTP requests for persons.
   * @param router Service that manages navigation between components.
   */
  constructor(private teamService: TeamService, private personService: PersonService, private router: Router) {
  }


  /**
   * Called at the init of the component.
   * Create form for team creation.
   */
  ngOnInit(): void {
    this.createTeamForm();
  }


  /**
   * Creates team if form is valid.
   */
  createTeam() {
    if (this.teamForm.valid) {
      let teamName = this.teamForm.get("teamName")?.value;
      this.teamService.createTeam(teamName).subscribe({
        next: (data) => {
          if (data == true) {
            alert("Votre équipe a bien été créée");
            this.personService.getRightInfos().subscribe({
              next: (data) => {
                if (data !== "") {
                  this.personService.setRight(data)
                  this.router.navigate(['']);
                }
              },
              error: (err) => {
                alert("Impossible de récupérer les droits !");
              }
            });
          } else {
            alert("Votre équipe n'a pas été créée");
          }
        },
        error: (err) => {
          alert("Votre équipe n'a pas été créée");
        }
      });
    }
  }


  /**
   * Creates form for team creation.
   */
  createTeamForm() {
    this.teamForm = new FormGroup({
      teamName: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }


  /** Getter for team's name in form */
  get teamName() {
    return this.teamForm.get('teamName');
  }

}

import {Component, OnInit} from '@angular/core';
import {TeamService} from "../../services/team.service";
import {Person} from "../../models/person";
import {Team} from "../../models/team";


/**
 * TeamValidationComponent implements view of the team validation and its functions.
 *
 * @author Guillaume Etienne
 * @version 1.0
 * @since 08.05.2022
 */
@Component({
  selector: 'app-team-validation',
  templateUrl: './team-validation.component.html',
  styleUrls: ['./team-validation.component.css']
})
export class TeamValidationComponent implements OnInit {

  /** All teams in contest */
  public teams: Map<Team, Person[]> = new Map<Team, Person[]>()


  /**
   * Constructor of the TeamValidationComponent
   *
   * @param teamService Service that manages all HTTP requests for the team.
   */
  constructor(private teamService: TeamService) {
    /**
     let pers: Person[] = [];
     pers.push(new Person("aa.ch", "Etienne", "Gui"))
     pers.push(new Person("bb.ch", "Will", "Steve"))
     this.teams.set(new Team("Team1", "aa.ch"), pers);
     this.teams.set(new Team("Team2", "bb.ch"), pers);
     this.teams.set(new Team("Team3", "aa.ch"), pers);
     this.teams.set(new Team("Team4", "bb.ch"), pers);
     */
  }


  /**
   * Called at the init of the component.
   * Get all teams and participants of the contest.
   */
  ngOnInit(): void {
    this.teamService.getTeamsInscription().subscribe({
      next: (teams) => {
        if (teams) {
          for (let team of teams) {
            this.getTeamParticipant(team);
          }
        }
      },
      error: (err) => {
        alert("Erreur lors de la récupération des équipes !");
      }
    });
  }


  /**
   * Get team's participants.
   *
   * @param team Team
   */
  getTeamParticipant(team: Team) {
    this.teamService.getTeamParticipants(team.teamName).subscribe({
      next: (participants) => {
        if (participants) {
          this.teams.set(team, participants)
        }
      },
      error: (err) => {
        alert("Erreur lors de la récupération des participants !");
      }
    });
  }


  /**
   * Accept a team in the contest.
   *
   * @param team Team's name
   */
  acceptTeam(team: string) {
    this.teamService.acceptTeam(team).subscribe({
      next: (data) => {
        if (data) {
          alert("Equipe accepté !");
          this.teams.delete(this.getTeamIndex(team));
        } else {
          alert("Erreur lors de l'acceptation du participant !");
        }
      },
      error: (err) => {
        alert("Erreur");
      }
    });
  }


  /**
   * Refuse a team in the contest.
   *
   * @param team Team's name
   */
  refuseTeam(team: string) {
    this.teamService.refuseTeam(team).subscribe({
      next: (data) => {
        if (data) {
          alert("Equipe refusée !");
          let teamIndex
          this.teams.delete(this.getTeamIndex(team));
        } else {
          alert("Erreur lors de l'acceptation de l'équipe !");
        }
      },
      error: (err) => {
        alert("Erreur");
      }
    });
  }


  /**
   * Get a team in list that corresponds to team's name.
   *
   * @param teamName Team's name
   */
  getTeamIndex(teamName: string) {
    let allTeams = this.teams.keys();
    let teamIndex: Team = allTeams.next().value

    for (let team of allTeams) {
      if (team.teamName === teamName) {
        teamIndex = team;
      }
    }
    return teamIndex;
  }

}

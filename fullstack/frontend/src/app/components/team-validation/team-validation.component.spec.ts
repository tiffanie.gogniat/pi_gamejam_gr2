import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamValidationComponent } from './team-validation.component';

describe('TeamValidationComponent', () => {
  let component: TeamValidationComponent;
  let fixture: ComponentFixture<TeamValidationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamValidationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

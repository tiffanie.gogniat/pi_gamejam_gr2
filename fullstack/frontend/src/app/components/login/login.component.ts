import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PersonService} from "../../services/person.service";
import {Router} from "@angular/router";
import {CookieService} from 'ngx-cookie-service';


/**
 * LoginComponent implements view of the login and its functions.
 *
 * @author Guillaume Etienne
 * @version 2.1
 * @since 28.03.2022
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /** Form for login */
  public loginForm!: FormGroup;

  /** Data received login request */
  private data: any;


  /**
   * Constructor of the LoginComponent
   *
   * @param personService Service that manages all HTTP requests for persons.
   * @param router Service that manages navigation between components.
   */
  constructor(private personService: PersonService, private router: Router) {
  }


  /**
   * Called at the init of the component.
   * Create form for login.
   */
  ngOnInit(): void {
    this.createLoginForm();
  }


  /**
   * Connects the person to the application.
   */
  connect() {
    if (this.loginForm.valid) {
      let account = this.loginForm.get("account")?.value;
      let password = this.loginForm.get("password")?.value;
      this.personService.connect(account, password).subscribe({
        next: (data) => {
          if (data !== "") {
            this.personService.setUser(account)
            this.personService.setToken(data)
            this.personService.getRightInfos().subscribe({
              next: (data) => {
                if (data !== "") {
                  this.personService.setRight(data)
                  this.router.navigate(['']);
                }
              },
              error: (err) => {
                alert("Impossible de récupérer les droits !");
              }
            });
          } else {
            alert("Connexion impossible");
          }
        },
        error: (err) => {
          alert("Connexion impossible");
        }
      });
    }
  }


  /**
   * Creates form for login.
   */
  createLoginForm() {
    this.loginForm = new FormGroup({
      account: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
  }

  /** Getter for account in form */
  get accountField() {
    return this.loginForm.get('account');
  }


  /** Getter for password in form */
  get passwordField() {
    return this.loginForm.get('password');
  }

}

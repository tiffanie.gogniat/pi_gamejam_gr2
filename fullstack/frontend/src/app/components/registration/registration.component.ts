import {Component, OnInit} from '@angular/core';
import {Team} from "../../models/team";
import {TeamService} from "../../services/team.service";
import {PersonService} from "../../services/person.service";
import {Router} from "@angular/router";


/**
 * RegistrationComponent implements view of the registration for a team and its functions.
 *
 * @author Guillaume Etienne
 * @version 2.1
 * @since 28.03.2022
 */
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  /** All teams in contest */
  public teams: Team[] = [];

  /** Max size of a team */
  public teamSizeMax = 5;


  /**
   * Constructor of the RegistrationComponent
   *
   * @param teamService Service that manages all HTTP requests for the team.
   * @param personService Service that manages all HTTP requests for persons.
   * @param router Service that manages navigation between components.
   */
  constructor(private teamService: TeamService, private personService: PersonService, private router: Router) {
  }


  /**
   * Called at the init of the component.
   * Update teams.
   */
  ngOnInit(): void {
    this.updateTeams();
  }

  /**
   * Requests a team to integrate it.
   *
   * @param teamName Name of the team to integrate.
   */
  requestIntegration(teamName: string) {
    this.teamService.requestIntegration(teamName).subscribe({
      next: (data) => {
        if (data == true) {
          this.updateTeams();
          alert("La demande est effectuée !");
        } else {
          alert("La demande n'a pas pu être effectuées !");
        }
      },
      error: (err) => {
        alert("La demande n'a pas pu être effectuées !");

      }
    });
  }


  /**
   * Get all teams on current contest.
   */
  updateTeams() {
    this.teamService.getTeamsInscription().subscribe({
      next: (data) => {
        this.teams = data;
      },
      error: (err) => {
      }
    });
  }


}

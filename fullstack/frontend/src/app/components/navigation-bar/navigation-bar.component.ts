import {Component, OnInit} from '@angular/core';
import {PersonService} from "../../services/person.service";
import {Router} from "@angular/router";
import {TeamService} from "../../services/team.service";
import {ContestService} from "../../services/contest.service";
import {AuthGuard} from "../../auth.guard";


/**
 * NavigationBarComponent implements view of the navigation bar and its functions.
 *
 * @author Guillaume Etienne
 * @version 2.1
 * @since 28.03.2022
 */
@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  /** Is current person connected */
  public isConnected: boolean = false;

  /** Is current person a team leader */
  public isLeader: boolean = false;


  /**
   * Constructor of the NavigationBarComponent
   *
   * @param personService Service that manages all HTTP requests for persons.
   * @param teamService Service that manages all HTTP requests for the team.
   * @param router Service that manages navigation between components.
   */
  constructor(private personService: PersonService, private teamService: TeamService,
              private contestService: ContestService, private authGuard: AuthGuard, private router: Router) {
  }

  /**
   * Called at the init of the component.
   * Refreshes the view.
   */
  ngOnInit(): void {
    this.refresh();
  }


  /**
   * Disconnects the person to the application.
   */
  disconnect() {
    this.personService.disconnect().subscribe({
      next: (data) => {
        console.log(data)
        if (data) {
          this.router.navigate(['']);
        }
        this.personService.deleteUser();
      },
      error: (err) => {
        alert("Déconnexion impossible");
      }
    });
  }


  /**
   * Open a new tab to show PDF of winners.
   */
  public openPDF() {
    this.contestService.openPDFWinners();
  }


  /**
   * Check path to show or not a button.
   */
  canActivate(path: string) {
    return this.authGuard.checkPath(path);
  }


  /**
   * Refreshes person's data from server.
   */
  public refresh() {
    this.isConnected = this.personService.isLogged();
    if (this.isConnected) {
      this.teamService.getTeamName().subscribe({
        next: teamName => {
          this.isLeader = teamName != "";
        }
      });
    }
  }

}

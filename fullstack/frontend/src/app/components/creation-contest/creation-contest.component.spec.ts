import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationContestComponent } from './creation-contest.component';

describe('CreationContestComponent', () => {
  let component: CreationContestComponent;
  let fixture: ComponentFixture<CreationContestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreationContestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

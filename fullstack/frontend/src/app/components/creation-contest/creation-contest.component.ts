import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {XMLBuilder, XMLParser} from "fast-xml-parser";
import {Contest} from "../../models/contest";
import {ContestService} from "../../services/contest.service";
import {Router} from "@angular/router";


/**
 * CreationContestComponent implements view of the contest creation and update and its functions.
 *
 * @author Guillaume Etienne
 * @version 1.0
 * @since 06.05.2022
 */
@Component({
  selector: 'app-creation-contest',
  templateUrl: './creation-contest.component.html',
  styleUrls: ['./creation-contest.component.css']
})
export class CreationContestComponent implements OnInit {

  /** Form for contest */
  public contestForm!: FormGroup;

  /** Current contest */
  private contest: Contest;


  /**
   * Constructor of the CreationContestComponent.
   *
   * @param contestService Service that manages all HTTP requests for the contest.
   * @param router Service that manages navigation between components.
   */
  constructor(private contestService: ContestService, private router: Router) {
    this.contest = contestService.getContest();
  }


  /**
   * Called at the init of the component.
   * Creates contest form.
   */
  ngOnInit(): void {
    this.createContestForm();
  }

  updateContest() {
    this.contestService.getContestInfos().subscribe({
      next: (contest) => {
        if (contest != null) {
          this.contest = contest;
          this.contestService.setContest(contest);
          window.location.reload();
        } else {
          this.contestService.setContest(new Contest())
        }
      }
    })
  }


  /**
   * Creates or update contest if form is valid.
   */
  createContest() {
    if (this.contestForm.valid) {
      let name = this.contestForm.get("name")?.value;
      let theme = this.contestForm.get("theme")?.value;
      let startContest = this.contestForm.get("startContest")?.value;
      let endContest = this.contestForm.get("endContest")?.value;
      let startRegistration = this.contestForm.get("startRegistration")?.value;
      let endRegistration = this.contestForm.get("endRegistration")?.value;
      let descriptionXML = this.parseDescriptionToXML();

      this.contest = new Contest(name, descriptionXML, theme, startContest, endContest, startRegistration, endRegistration, this.contest.counter);
      this.contestService.updateContest(this.contest).subscribe({
        next: (data) => {
          if (data) {
            alert("Le concours a été mis à jour !");
            this.router.navigate(['']);
          } else {
            this.updateContest();
            alert("Le concours n'a pas été mis à jour !");
          }
        },
        error: (err) => {
          this.updateContest();
          alert("Le concours n'a pas été mis à jour !");
        }
      });
    }
  }


  /**
   * Parse description data to xml.
   *
   * @return Description in xml format.
   */
  parseDescriptionToXML() {
    let title = this.contestForm.get("title")?.value;
    let subtitle = this.contestForm.get("subtitle")?.value;
    let src = this.contestForm.get("image")?.value;
    let alt = this.contestForm.get("alt")?.value;
    let comment = this.contestForm.get("comment")?.value;

    let jObj = {
      "description": {
        "title": title,
        "subtitle": subtitle,
        "comment": comment,
        "image": {
          "_src": src,
          "_alt": alt,
        }
      }
    };
    const options = {
      ignoreAttributes: false,
      attributeNamePrefix: "_"
    };
    const builder = new XMLBuilder(options);
    return builder.build(jObj);
  }


  /**
   * Creates form for contest.
   */
  createContestForm() {
    let contestDescription = {title: '', subtitle: '', image: {src: '', alt: ''}, comment: ''};
    if (this.contest.descriptionXml !== "") {
      const options = {
        ignoreAttributes: false,
        attributeNamePrefix: ""
      };
      const parser = new XMLParser(options);
      if (this.contest?.descriptionXml != null) {
        contestDescription = parser.parse(this.contest?.descriptionXml).description;
      }
    }

    this.contestForm = new FormGroup({
      name: new FormControl(this.contest.contestName, [
        Validators.required
      ]),
      theme: new FormControl(this.contest.theme, [
        Validators.required
      ]),
      title: new FormControl(contestDescription.title, [
        Validators.required
      ]),
      subtitle: new FormControl(contestDescription.subtitle, [
        Validators.required
      ]),
      image: new FormControl(contestDescription.image.src, [
        Validators.required
      ]),
      alt: new FormControl(contestDescription.image.alt, [
        Validators.required
      ]),
      comment: new FormControl(contestDescription.comment, [
        Validators.required
      ]),
      startContest: new FormControl(this.contest.beginContest?.slice(0, -13), [
        Validators.required
      ]),
      endContest: new FormControl(this.contest.endContest?.slice(0, -13), [
        Validators.required
      ]),
      startRegistration: new FormControl(this.contest.beginRegister?.slice(0, -13), [
        Validators.required
      ]),
      endRegistration: new FormControl(this.contest.endRegister?.slice(0, -13), [
        Validators.required
      ])
    });
  }


  /** Getter for name in form */
  get nameField() {
    return this.contestForm.get('name');
  }

  /** Getter for theme in form */
  get themeField() {
    return this.contestForm.get('theme');
  }


  /** Getter for title in form */
  get titleField() {
    return this.contestForm.get('title');
  }


  /** Getter for subtitle in form */
  get subtitleField() {
    return this.contestForm.get('subtitle');
  }


  /** Getter for image in form */
  get imageField() {
    return this.contestForm.get('image');
  }


  /** Getter for image in form */
  get altField() {
    return this.contestForm.get('alt');
  }


  /** Getter for comment in form */
  get commentField() {
    return this.contestForm.get('comment');
  }


  /** Getter for comment in form */
  get startContestField() {
    return this.contestForm.get('startContest');
  }


  /** Getter for comment in form */
  get endContestField() {
    return this.contestForm.get('endContest');
  }


  /** Getter for comment in form */
  get startRegistrationField() {
    return this.contestForm.get('startRegistration');
  }


  /** Getter for comment in form */
  get endRegistrationField() {
    return this.contestForm.get('endRegistration');
  }

}

export class TeamComposition {
  private _teamName: string;
  private _lastname: string;
  private _firstname: string;
  private _participant: string;
  private _validated: boolean;

  constructor(
    teamName?: string,
    lastname?: string,
    firstname?: string,
    participant?: string,
    validated?: boolean
  ) {
    this._teamName = teamName || '';
    this._lastname = lastname || '';
    this._firstname = firstname || '';
    this._participant = participant || '';
    this._validated = validated || false;
  }

  get teamName(): string {
    return this._teamName;
  }

  set teamName(value: string) {
    this._teamName = value;
  }

  get lastname(): string {
    return this._lastname;
  }

  set lastname(value: string) {
    this._lastname = value;
  }

  get firstname(): string {
    return this._firstname;
  }

  set firstname(value: string) {
    this._firstname = value;
  }

  get participant(): string {
    return this._participant;
  }

  set participant(value: string) {
    this._participant = value;
  }

  get validated(): boolean {
    return this._validated;
  }

  set validated(value: boolean) {
    this._validated = value;
  }
}

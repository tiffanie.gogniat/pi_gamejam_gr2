import {Person} from "./person";

export class Manager {
  private _person: Person;

  constructor(
    person?: Person
  ) {
    this._person = person || new Person();
  }

  get mail(): string {
    return this._person.mail;
  }

  set mail(mail: string) {
    this._person.mail = mail;
  }

  get lastname(): string {
    return this._person.lastname;
  }

  set lastname(lastname: string) {
    this._person.lastname = lastname;
  }

  get firstname(): string {
    return this._person.firstname;
  }

  set firstname(firstname: string) {
    this._person.firstname = firstname;
  }

  get pwd(): string {
    return this._person.pwd;
  }

  set pwd(pwd: string) {
    this._person.pwd = pwd;
  }
}

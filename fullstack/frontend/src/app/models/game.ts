export class Game {
  private _gameName: string;
  private _filepath: string;
  private _originality: number;
  private _design: number;
  private _playability: number;


  constructor(
    gameName?: string,
    filepath?: string,
    originality?: number,
    design?: number,
    playability?: number,
  ) {
    this._gameName = gameName || '';
    this._filepath = filepath || '';
    this._originality = originality || 1;
    this._design = design || 1;
    this._playability = playability || 1;
  }

  get gameName(): string {
    return this._gameName;
  }

  set gameName(value: string) {
    this._gameName = value;
  }

  get filepath(): string {
    return this._filepath;
  }

  set filepath(value: string) {
    this._filepath = value;
  }

  get originality(): number {
    return this._originality;
  }

  set originality(value: number) {
    this._originality = value;
  }

  get design(): number {
    return this._design;
  }

  set design(value: number) {
    this._design = value;
  }

  get playability(): number {
    return this._playability;
  }

  set playability(value: number) {
    this._playability = value;
  }
}

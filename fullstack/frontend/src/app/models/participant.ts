import {Person} from "./person";

export class Participant {
  private _person: Person;
  private _birthdate: Date;
  private _address: string;
  private _zip: number;
  private _locality: string;

  constructor(
    person?: Person,
    birthdate?: Date,
    address?: string,
    zip?: number,
    locality?: string
  ) {
    this._person = person || new Person();
    this._birthdate = birthdate || new Date();
    this._address = address || '';
    this._zip = zip || 1000;
    this._locality = locality || '';
  }

  get mail(): string {
    return this._person.mail;
  }

  set mail(mail: string) {
    this._person.mail = mail;
  }

  get lastname(): string {
    return this._person.lastname;
  }

  set lastname(lastname: string) {
    this._person.lastname = lastname;
  }

  get firstname(): string {
    return this._person.firstname;
  }

  set firstname(firstname: string) {
    this._person.firstname = firstname;
  }

  get pwd(): string {
    return this._person.pwd;
  }

  set pwd(pwd: string) {
    this._person.pwd = pwd;
  }

  get birthdate(): Date {
    return this._birthdate;
  }

  set birthdate(value: Date) {
    this._birthdate = value;
  }

  get address(): string {
    return this._address;
  }

  set address(value: string) {
    this._address = value;
  }

  get zip(): number {
    return this._zip;
  }

  set zip(value: number) {
    this._zip = value;
  }

  get locality(): string {
    return this._locality;
  }

  set locality(value: string) {
    this._locality = value;
  }

}

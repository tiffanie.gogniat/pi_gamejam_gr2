export class Contest {
  private _contestName: string;
  private _descriptionXml: string;
  private _theme: string;
  private _beginContest: string;
  private _endContest: string;
  private _beginRegister: string;
  private _endRegister: string;
  private _counter: number;

  constructor(
    contestName?: string,
    descriptionXml?: string,
    theme?: string,
    beginContest?: string,
    endContest?: string,
    beginRegister?: string,
    endRegister?: string,
    counter?: number
  ) {
    this._contestName = contestName || "";
    this._descriptionXml = descriptionXml || "";
    this._theme = theme || "";
    this._beginContest = beginContest || "";
    this._endContest = endContest || "";
    this._beginRegister = beginRegister || "";
    this._endRegister = endRegister || "";
    this._counter = counter || 0;
  }

  get contestName(): string {
    return this._contestName;
  }

  set contestName(value: string) {
    this._contestName = value;
  }

  get descriptionXml(): string {
    return this._descriptionXml;
  }

  set descriptionXml(value: string) {
    this._descriptionXml = value;
  }

  get theme(): string {
    return this._theme;
  }

  set theme(value: string) {
    this._theme = value;
  }

  get beginContest(): string {
    return this._beginContest;
  }

  set beginContest(value: string) {
    this._beginContest = value;
  }

  get endContest(): string {
    return this._endContest;
  }

  set endContest(value: string) {
    this._endContest = value;
  }

  get beginRegister(): string {
    return this._beginRegister;
  }

  set beginRegister(value: string) {
    this._beginRegister = value;
  }

  get endRegister(): string {
    return this._endRegister;
  }

  set endRegister(value: string) {
    this._endRegister = value;
  }

  get counter(): number {
    return this._counter;
  }

  set counter(value: number) {
    this._counter = value;
  }
}

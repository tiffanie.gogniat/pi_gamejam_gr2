export class Person {
  private _mail: string;
  private _lastname: string;
  private _firstname: string;
  private _pwd: string;

  constructor(
    mail?: string,
    lastname?: string,
    firstname?: string,
    pwd?: string
  ) {
    this._mail = mail || '';
    this._lastname = lastname || '';
    this._firstname = firstname || '';
    this._pwd = pwd || '';
  }

  get mail(): string {
    return this._mail;
  }

  set mail(value: string) {
    this._mail = value;
  }

  get lastname(): string {
    return this._lastname;
  }

  set lastname(value: string) {
    this._lastname = value;
  }

  get firstname(): string {
    return this._firstname;
  }

  set firstname(value: string) {
    this._firstname = value;
  }

  get pwd(): string {
    return this._pwd;
  }

  set pwd(value: string) {
    this._pwd = value;
  }
}

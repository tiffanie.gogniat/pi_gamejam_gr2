import {Jury} from "./jury";

export class Vote {
  private _jury: string;
  private _game: string;
  private _notesJSON: {};

  constructor(
    jury: string,
    game: string,
    notesJSON: {}
  ) {
    this._jury = jury;
    this._game = game;
    this._notesJSON = notesJSON;
  }


  get jury(): string {
    return this._jury;
  }

  set jury(value: string) {
    this._jury = value;
  }

  get game(): string {
    return this._game;
  }

  set game(value: string) {
    this._game = value;
  }

  get notesJSON(): {} {
    return this._notesJSON;
  }

  set notesJSON(value: {}) {
    this._notesJSON = value;
  }
}

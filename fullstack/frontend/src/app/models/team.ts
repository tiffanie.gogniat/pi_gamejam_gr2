import {Participant} from "./participant";
import {Game} from "./game";
import {TeamComposition} from "./teamComposition";

export class Team {
  private _teamName: string;
  private _leader: string;
  private _game: Game;
  private _participants: TeamComposition[];

  constructor(
    teamName?: string,
    leader?: string,
    game?: Game,
    participants?: TeamComposition[]
  ) {
    this._teamName = teamName || '';
    this._leader = leader || '';
    this._game = game || new Game();
    this._participants = participants || [];
  }

  addParticipant(teamComposition: TeamComposition) {
    this._participants.push(teamComposition);
  }

  countValidatedParticipant() {
    let nb = 0;
    for (let partcipinant of this._participants) {
      if (partcipinant.validated) nb++;
    }
    return nb;
  }

  get teamName(): string {
    return this._teamName;
  }

  set teamName(value: string) {
    this._teamName = value;
  }

  get leader(): string {
    return this._leader;
  }

  set leader(value: string) {
    this._leader = value;
  }

  get game(): Game {
    return this._game;
  }

  set game(value: Game) {
    this._game = value;
  }

  get participants(): TeamComposition[] {
    return this._participants;
  }

  set participants(value: TeamComposition[]) {
    this._participants = value;
  }
}

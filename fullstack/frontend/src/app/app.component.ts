import { Component } from '@angular/core';
import {PersonService} from "./services/person.service";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';

  constructor(private personService: PersonService, private cookieService: CookieService) {
  }

  ngOnInit(): void {
  }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";
import {CookieService} from "ngx-cookie-service";
import {Contest} from "../models/contest";
import {PersonService} from "./person.service";


/**
 * ContestService implements all HTTP requests for contest.
 *
 * @author Guillaume Etienne
 * @version 1.1
 * @since 03.04.2022
 */
@Injectable({
  providedIn: 'root'
})
export class ContestService {

  /** URL of the API */
  private apiUrl: string;

  /**
   * Consctuctor of the ContestService
   *
   * @param http Service that manages HTTP Requests.
   * @param cookieService Service that manages cookies.
   */
  constructor(private http: HttpClient, private personService: PersonService, private cookieService: CookieService) {
    this.apiUrl = environment.apiUrl;
  }


  /**
   * Connects the person to the application.
   *
   * @return Observable<string> request
   */
  updateContest(contest: Contest) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/updateContest", {"contest": contest}, {headers: headers})
      .pipe(catchError(error => this.handleError(error))
      );
  }


  /**
   * Get current contest.
   *
   * @return Observable<any> request
   */
  getContestInfos() {
    return this.http.get<Contest>(this.apiUrl + "/getContest").pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Get contest from cookie.
   *
   * @return contest
   */
  getContest() {
    let contest: Contest = JSON.parse(this.cookieService.get("contest"));
    return contest;
  }


  /**
   * Set contest in cookie.
   *
   * @param contest contest
   */
  setContest(contest: Contest) {
    this.cookieService.set("contest", JSON.stringify(contest));
  }


  /**
   * Open a new tab to show PDF of winners.
   */
  openPDFWinners() {
    window.open(this.apiUrl + "/generatePDF", '_blank')?.focus();
  }

// get from https://angular.io/guide/http#making-a-post-request
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}

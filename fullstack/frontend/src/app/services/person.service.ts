import {Injectable} from '@angular/core';
import {catchError} from "rxjs/operators";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {throwError} from "rxjs";
import {CookieService} from "ngx-cookie-service";


/**
 * PersonService implements all HTTP requests for person (Participant, Jury, Manager).
 *
 * @author Guillaume Etienne
 * @version 1.1
 * @since 03.04.2022
 */
@Injectable({
  providedIn: 'root'
})
export class PersonService {

  /** URL of the API */
  private apiUrl: string;


  /**
   * Constructor of the PersonService
   *
   * @param http Service that manages HTTP Requests.
   * @param cookieService Service that manages cookies.
   */
  constructor(private http: HttpClient, private cookieService: CookieService) {
    this.apiUrl = environment.apiUrl;
  }


  /**
   * Connects the person to the application.
   *
   * @return Observable<string> request
   */
  connect(mail: string, password: string) {
    return this.http.post(this.apiUrl + "/connect", {
      "email": mail,
      "pwd": password
    }, {responseType: 'text'}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Disconnects the person to the application.
   *
   * @return Observable<string> request
   */
  disconnect() {
    const headers = new HttpHeaders({
      "email": this.getUser(),
      "token": this.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/disconnect", "", {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Get right of user.
   *
   * @return Observable<string> request
   */
  getRightInfos() {
    const headers = new HttpHeaders({
      "email": this.getUser(),
      "token": this.getToken()
    });
    return this.http.get(this.apiUrl + "/getRight", {headers: headers, responseType: "text"}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Get logged state of person.
   *
   * @return boolean true if logged, else false
   */
  isLogged() {
    return (this.getUser() !== "" && this.getToken() !== "");
  }


  /**
   * Delete user in cookie.
   */
  deleteUser() {
    this.cookieService.set("user", "");
    this.cookieService.set("token", "");
  }


  /**
   * Get user from cookie.
   *
   * @return user's email
   */
  getUser() {
    let user = this.cookieService.get("user");
    return user;
  }


  /**
   * Set user's email in cookie.
   *
   * @param user
   */
  setUser(user: string) {
    this.cookieService.set("user", user);
  }


  /**
   * Get token from cookie.
   *
   * @return user's token
   */
  getToken() {
    let user = this.cookieService.get("token");
    return user;
  }


  /**
   * Set user's token in cookie.
   *
   * @param user user's token
   */
  setToken(token: string) {
    this.cookieService.set("token", token);
  }


  /**
   * Get right from cookie.
   *
   * @return user's right
   */
  getRight() {
    let right = this.cookieService.get("right");
    return right;
  }


  /**
   * Set user's right in cookie.
   *
   * @param right user's right
   */
  setRight(right: string) {
    this.cookieService.set("right", right);
  }

// get from https://angular.io/guide/http#making-a-post-request
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}


import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";

import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {environment} from "../../environments/environment";
import {Team} from "../models/team";
import {TeamComposition} from "../models/teamComposition";
import {PersonService} from "./person.service";
import {ContestService} from "./contest.service";
import {Vote} from "../models/vote";
import {Game} from "../models/game";
import {Person} from "../models/person";


/**
 * TeamService implements all HTTP requests for team.
 *
 * @author Guillaume Etienne
 * @version 2.1
 * @since 03.04.2022
 */
@Injectable({
  providedIn: 'root'
})
export class TeamService {

  /** URL of the API */
  private apiUrl: string;


  /**
   * Constructor of the TeamService
   *
   * @param http Service that manages HTTP Requests.
   * @param personService Service that manages all HTTP requests for persons.
   * @param contestService Service that manages all HTTP requests for the contest.
   */
  constructor(private http: HttpClient, private personService: PersonService, private contestService: ContestService) {
    this.apiUrl = environment.apiUrl;
  }


  /**
   * Uploads a game.
   *
   * @param gameName Game's name
   * @return Observable<string> request
   */
  uploadGame(gameName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/uploadGame", {
      "gameName": gameName
    }, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Creates a team.
   *
   * @param teamName Team's name
   * @return Observable<string> request
   */
  createTeam(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/createTeam", {
      "teamName": teamName,
      "contest": this.contestService.getContest().contestName,
      "game": ""
    }, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Gets team's name of leader
   *
   * @return Observable<string> request
   */
  getTeamName() {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.get(this.apiUrl + "/getTeamName", {headers: headers, responseType: "text"}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Gets all teams.
   *
   * @return Observable<Team[]> request
   */
  getTeamsInscription() {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.get<Team[]>(this.apiUrl + "/getTeamsInscription", {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Gets team's participants.
   *
   * @param teamName Team's name
   * @return Observable<Person[]> request
   */
  getTeamParticipants(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.get<Person[]>(this.apiUrl + "/getTeamParticipants/" + teamName, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Gets integration requests for a team.
   *
   * @param teamName Team's name
   * @return Observable<TeamComposition[]> request
   */
  getIntegrationRequests(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.get<TeamComposition[]>(this.apiUrl + "/getIntegrationRequests/" + teamName, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Gets all games.
   *
   * @return Observable<Game[]> request
   */
  getGames() {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.get<Game[]>(this.apiUrl + "/getGames", {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Gets validated integration requests for a team.
   *
   * @param teamName Team's name
   * @return Observable<Object> request
   */
  getIntegrationValidated(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.get(this.apiUrl + "/getIntegrationRequests/" + teamName, {
      headers: headers,
      responseType: "text"
    }).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Sends a request to integrate a team.
   *
   * @param teamName Team's name
   * @param mail Email of the participant
   * @return Observable<Object> request
   */
  requestIntegration(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/requestIntegration", {"teamName": teamName}, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Accepts a request to integrate a team.
   *
   * @param participant Email of the participant
   * @param teamName Team's name
   * @return Observable<Object> request
   */
  acceptRequestIntegration(participant: string, teamName: string) {
    console.log("Accept " + participant + " in team " + teamName);
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/acceptIntegrationRequest", {
      "teamName": teamName,
      "participant": participant
    }, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Refuses a request to integrate a team.
   *
   * @param participant Email of the participant
   * @param teamName Team's name
   * @return Observable<Object> request
   */
  refuseRequestIntegration(participant: string, teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/refuseIntegrationRequest", {
      "teamName": teamName,
      "participant": participant
    }, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Accepts a team.
   *
   * @param teamName Team's name
   * @return Observable<Object> request
   */
  acceptTeam(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/acceptTeam", {"teamName": teamName}, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Refuses a team.
   *
   * @param teamName Team's name
   * @return Observable<Object> request
   */
  refuseTeam(teamName: string) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/refuseTeam", {"teamName": teamName}, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  /**
   * Send votes from current jury.
   *
   * @param votes All votes for all game from a jury.
   * @return Observable<Object> request
   */
  sendVotes(votes: Vote[]) {
    const headers = new HttpHeaders({
      "email": this.personService.getUser(),
      "token": this.personService.getToken()
    });
    return this.http.post<boolean>(this.apiUrl + "/sendVotes", {"votes": votes}, {headers: headers}).pipe(
      catchError(error => this.handleError(error))
    );
  }


  // get from https://angular.io/guide/http#making-a-post-request
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }

}

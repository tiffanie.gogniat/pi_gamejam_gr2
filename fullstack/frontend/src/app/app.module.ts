import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from "@angular/forms";
import {AppRoutingModule} from './app-routing.module';
import {NavigationBarComponent} from './components/navigation-bar/navigation-bar.component';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {CreationTeamComponent} from './components/creation-team/creation-team.component';
import {PageTitleComponent} from './components/page-title/page-title.component';
import {IntegrationRequestsComponent} from './components/integration-requests/integration-requests.component';
import {HomeComponent} from "./components/home/home.component";
import {CookieService} from "ngx-cookie-service";

import {DatePipe, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {CreationContestComponent} from './components/creation-contest/creation-contest.component';
import {RatingGameComponent} from './components/rating-game/rating-game.component';
import {TeamValidationComponent} from './components/team-validation/team-validation.component';
import { UploadGameComponent } from './components/upload-game/upload-game.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    LoginComponent,
    RegistrationComponent,
    CreationTeamComponent,
    PageTitleComponent,
    IntegrationRequestsComponent,
    HomeComponent,
    CreationContestComponent,
    RatingGameComponent,
    TeamValidationComponent,
    UploadGameComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    CookieService,
    DatePipe,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

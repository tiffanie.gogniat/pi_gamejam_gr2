import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {PersonService} from "./services/person.service";
import {DatePipe} from '@angular/common';
import {ContestService} from "./services/contest.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private personService: PersonService, private contestService: ContestService, private datePipe: DatePipe) {
  }

  checkPath(path: string) {
    let right: string = this.personService.getRight();
    let now = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    let contest = this.contestService.getContest();

    if (now) {
      //if (!contest.beginRegister || now < contest.beginRegister) {
      if (path === "createContest" && right === "MANAGER") return true;

      //} else
      if (contest.beginRegister < now && now < contest.endRegister) {
        if (path === "integrationRequests" && right === "LEADER") return true;
        if (path === "registration" && right === "PARTICIPANT") return true;
        if (path === "createTeam" && right === "PARTICIPANT") return true;

      } else if (contest.endRegister < now && now < contest.beginContest) {
        if (path === "teamValidation" && right === "MANAGER") return true;

      } else if (contest.beginContest < now && now < contest.endContest) {
        if (path === "uploadGame" && right === "LEADER") return true;

      } else if (contest.endContest < now) {
        if (path === "ratingGame" && right === "JURY") return true;
        if (path === "openPDF") return true;
      }
    }

    return false;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let path: string | undefined = route.url.pop()?.path

    if (path) {
      return this.checkPath(path);
    }
    return false;
  }

}

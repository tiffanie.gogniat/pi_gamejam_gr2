-- drop the whole db before creating it (only because it is an exercise don't do that without check irl)
-- DROP DATABASE gamejamdb;
-- DROP USER gamejamadmin;
-- DROP USER gamejamwebuser;


/*****************
* Creating users *
*****************/

CREATE USER gamejamadmin WITH PASSWORD 'password';
CREATE USER gamejamwebuser WITH PASSWORD 'password';

CREATE DATABASE gamejamdb WITH template=template0 OWNER=gamejamadmin;
GRANT CONNECT ON DATABASE gamejamdb TO gamejamwebuser;

-- give privileges to the user 

ALTER DEFAULT privileges GRANT all ON tables TO gamejamadmin;
ALTER DEFAULT privileges GRANT all ON sequences TO gamejamadmin;
\c gamejamdb gamejamadmin 


/******************
* Creating tables *
******************/

CREATE TABLE person (
    mail VARCHAR(50) PRIMARY KEY NOT NULL,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    pwd TEXT NOT NULL
);
 
CREATE TABLE jury (
    person VARCHAR(50) PRIMARY KEY,
    FOREIGN KEY (person) REFERENCES person (mail)
);

CREATE TABLE manager (
    person VARCHAR(50) PRIMARY KEY,
    FOREIGN KEY (person) REFERENCES person (mail)
);

CREATE TABLE participant (
    person VARCHAR(50) PRIMARY KEY,
    birthdate DATE NOT NULL,
    addr VARCHAR(50),
    zip INT,
    locality VARCHAR(50),
    FOREIGN KEY (person) REFERENCES person (mail)
);

CREATE TABLE game (
    gameName VARCHAR(50) PRIMARY KEY NOT NULL,
    filepath VARCHAR(50)
);

CREATE TABLE contest (
    contestName VARCHAR(50) PRIMARY KEY NOT NULL DEFAULT 'Lorem ipsum',
    theme VARCHAR(50),
    descriptionXML XML,
    beginRegister TIMESTAMP,
    endRegister TIMESTAMP,
    beginContest TIMESTAMP,
    endContest TIMESTAMP,
    counter INT NOT NULL DEFAULT 0,
    CONSTRAINT checkDates CHECK (endContest > beginContest 
                                    AND beginContest > endRegister 
                                    AND endRegister > beginRegister) -- C1
);

CREATE TABLE team (
    teamName VARCHAR(50) PRIMARY KEY NOT NULL,
    leader VARCHAR(50) NOT NULL,
    contest VARCHAR(50) NOT NULL,
    game VARCHAR(50),
    validated BOOLEAN DEFAULT FALSE,
    requestValidation BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (game) REFERENCES game (gameName),
    FOREIGN KEY (leader) REFERENCES participant (person),
    FOREIGN KEY (contest) REFERENCES contest (contestName)
);

CREATE TABLE teamComposition (
    teamName VARCHAR(50),
    participant VARCHAR(50),
    validatedParticipant BOOLEAN DEFAULT NULL,
    counter INT NOT NULL DEFAULT 0,
    FOREIGN KEY (participant) REFERENCES participant (person),
    FOREIGN KEY (teamName) REFERENCES team (teamName),
    PRIMARY KEY (teamName, participant)
);

CREATE TABLE vote (
    gameName VARCHAR(50) NOT NULL,
    jury VARCHAR(50) NOT NULL,
    notesJson TEXT,
    counter INT NOT NULL DEFAULT 0,
    FOREIGN KEY (jury) REFERENCES jury (person),
    FOREIGN KEY (gameName) REFERENCES game (gameName),
    PRIMARY KEY (gameName, jury)
);

CREATE TABLE contestManagement (
    contestName VARCHAR(50),
    manager VARCHAR(50),
    FOREIGN KEY (manager) REFERENCES manager (person),
    FOREIGN KEY (contestName) REFERENCES contest (contestName),
    PRIMARY KEY (contestName, manager)
);


/**********
* Indexes *
**********/

CREATE INDEX idx_person_mail_password ON person(mail, pwd);  -- ajouter pour optimiser le login
CREATE INDEX idx_teamComposition_teamName_participant ON teamComposition(teamName, participant);  -- ajouter pour optimiser la recherche des membres d'une team et les comparaison en relation avec
CREATE INDEX idx_vote_gameName_notesJson ON vote(gameName, notesJson);  -- ajouter pour optimiser la recherche lors de la création du score (comme vu dans la partie 1.1.1)

-- give privileges to the webuser once table created

GRANT SELECT ON ALL TABLES IN SCHEMA public TO gamejamwebuser;
--ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO gamejamwebuser;
GRANT INSERT, UPDATE, DELETE ON contest, team, game, teamcomposition TO gamejamwebuser;
GRANT INSERT ON vote TO gamejamwebuser;


-- inserting the default values

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('jury.test@mail.com', 'jury', 'einstein', 'd8ee4520789979bc056d57171dea9b65'); 

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('jury2.test@mail.com', 'judge', 'super', '31cfe8b0d12a77ac89cceb97551602a2'); 

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('jury3.test@mail.com', 'roger', 'super', '2c215de126f03b36620c047ff3c0fb15'); 

INSERT INTO jury (person)
VALUES ('jury.test@mail.com');

INSERT INTO jury (person)
VALUES ('jury2.test@mail.com');

INSERT INTO jury (person)
VALUES ('jury3.test@mail.com');

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('manager.test@mail.com', 'manager', 'claude', '5d61cb1d01a65cf0115eb1cb8213e983');

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('manager2.test@mail.com', 'manager', 'claude', 'b20686a42a18c07465e7652177972f78');

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('manager3.test@mail.com', 'manager', 'claude', '567b29a0f931632aaf1d4e2c989e717f');

INSERT INTO manager (person)
VALUES ('manager.test@mail.com');

INSERT INTO manager (person)
VALUES ('manager2.test@mail.com');

INSERT INTO manager (person)
VALUES ('manager3.test@mail.com');

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('participant.test@mail.com', 'participant', 'test', 'f458917c9ae328bd3c3039199b069826');

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('participant2.test@mail.com', 'participant2', 'test2', 'b1061a0f19adce4e278ce2ea19b1a2cd'); 

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('participant3.test@mail.com', 'participant2', 'test2', '1b6f29e06724d4f831507561f5876fcd'); 

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('participant4.test@mail.com', 'participant2', 'test2', 'f2a7265c8b3d25075bcd0f5cea08ed06'); 

INSERT INTO person (mail, firstname, lastname, pwd)
VALUES ('participant5.test@mail.com', 'participant2', 'test2', 'ef8fd2c8715b0828c9aa484510d78214'); 

INSERT INTO participant (person, birthdate, addr, zip, locality)
VALUES ('participant.test@mail.com', TO_DATE('17/12/1999', 'DD/MM/YYYY'), 'Rue du moulin 1', 1700, 'Fribourg'); 

INSERT INTO participant (person, birthdate, addr, zip, locality)
VALUES ('participant2.test@mail.com', TO_DATE('09/09/1999', 'DD/MM/YYYY'), 'Rue du moulin 2', 1700, 'Fribourg'); 

INSERT INTO participant (person, birthdate, addr, zip, locality)
VALUES ('participant3.test@mail.com', TO_DATE('17/12/1988', 'DD/MM/YYYY'), 'Rue Martin 1', 1700, 'Fribourg'); 

INSERT INTO participant (person, birthdate, addr, zip, locality)
VALUES ('participant4.test@mail.com', TO_DATE('09/09/1985', 'DD/MM/YYYY'), 'Rue du moulin 3', 1700, 'Fribourg'); 

INSERT INTO participant (person, birthdate, addr, zip, locality)
VALUES ('participant5.test@mail.com', TO_DATE('09/09/1978', 'DD/MM/YYYY'), 'Rue du moulin 19', 1700, 'Fribourg'); 


/********
* Views *
********/

CREATE VIEW participantView as
SELECT firstname, lastname, mail, birthdate, addr, zip, locality
FROM person
RIGHT JOIN participant ON person.mail = participant.person;

CREATE VIEW juryView as
SELECT firstname, lastname, mail
FROM person
NATURAL JOIN jury;

CREATE VIEW managerView as
SELECT firstname, lastname, mail
FROM person
NATURAL JOIN manager;


/***********
* Triggers *
***********/

-- Check there is only one team leader per teams

-- C2 : Les votes ne peuvent être créés que lorsque le concours est terminé

CREATE OR REPLACE FUNCTION fcCheckVotesValidity() RETURNS TRIGGER AS $fcCheckVotesValidity$
DECLARE
	endC TIMESTAMP;
BEGIN
	SELECT endContest into endC FROM contest;
	IF endC > NOW() THEN
		RAISE EXCEPTION 'You can not vote for a Contest before it has ended';
	END IF;
    RETURN NEW;
END;
$fcCheckVotesValidity$ LANGUAGE plpgsql;

CREATE TRIGGER trCheckVotesValidity BEFORE INSERT ON vote
FOR EACH ROW EXECUTE FUNCTION fcCheckVotesValidity();

-- C3 : Un participant ne peut être accepté que dans une seule équipe

CREATE OR REPLACE FUNCTION fcCheckParticipantIsInOneTeamOnly() RETURNS TRIGGER AS $fcCheckParticipantIsInOneTeamOnly$
DECLARE
	nb INT;
BEGIN
	nb := (SELECT count(*) FROM teamComposition WHERE participant = NEW.participant AND validatedParticipant = TRUE);
	IF nb != 0 THEN
		RAISE EXCEPTION 'A participant can only be in one team.';
	END IF;
    RETURN NEW;
END;
$fcCheckParticipantIsInOneTeamOnly$ LANGUAGE plpgsql;

CREATE TRIGGER trCheckParticipantIsInOneTeamOnly BEFORE INSERT OR UPDATE ON teamComposition
FOR EACH ROW EXECUTE FUNCTION fcCheckParticipantIsInOneTeamOnly();

-- C4 : Seulement 4 participants peuvent être validés pour une équipe

CREATE OR REPLACE FUNCTION fcCheckParticipantNumbers() RETURNS TRIGGER AS $fcCheckParticipantNumbers$
DECLARE
	nbParticipant INT;
BEGIN
    nbParticipant := (SELECT count(*) FROM teamComposition WHERE teamName = NEW.teamName);
	IF nbParticipant = 4 THEN
		RAISE EXCEPTION 'A team can only have 4 participant';
	END IF;
    RETURN NEW;
END;
$fcCheckParticipantNumbers$ LANGUAGE plpgsql;

CREATE TRIGGER trCheckParticipantNumbers BEFORE UPDATE OR INSERT ON teamComposition
FOR EACH ROW EXECUTE FUNCTION fcCheckParticipantNumbers();

-- C5 : Un chef d'équipe ne peut pas être un participant pour le même concours

CREATE OR REPLACE FUNCTION fcCheckParticipantNotLeader() RETURNS TRIGGER AS $fcCheckParticipantNotLeader$
DECLARE
	teamLeader VARCHAR(50);
BEGIN
	SELECT leader into teamLeader FROM team WHERE teamName = NEW.teamName;
	IF teamLeader = NEW.participant THEN
		RAISE EXCEPTION 'A team leader can not be a participant in the same contest';
	END IF;
    RETURN NEW;
END;
$fcCheckParticipantNotLeader$ LANGUAGE plpgsql;

CREATE TRIGGER trCheckParticipantNotLeader BEFORE INSERT OR UPDATE ON teamComposition
FOR EACH ROW EXECUTE FUNCTION fcCheckParticipantNotLeader();

-- CR1, CR2 Une entrée dans la table Person ne peut apparaître que dans une seule table fille (Manager, Jury ou Participant) et la table person

CREATE OR REPLACE FUNCTION fcCheckPersonHasOnlyOneRole() RETURNS TRIGGER AS $fcCheckPersonHasOnlyOneRole$
DECLARE
	nb INT;
BEGIN
	nb:= (SELECT count(*) FROM participant WHERE person = NEW.person) +
	     (SELECT count(*) FROM jury WHERE person = NEW.person) +
         (SELECT count(*) FROM manager WHERE person = NEW.person) +
         (SELECT count(*) FROM person WHERE mail = NEW.person) ;
	IF nb != 2 THEN
		RAISE EXCEPTION 'Person must have a role: participant, jury or manager.';
	END IF;
    RETURN NEW;
END;
$fcCheckPersonHasOnlyOneRole$ LANGUAGE plpgsql;

CREATE TRIGGER trCheckParticipantHasOnlyOneRole AFTER INSERT OR UPDATE ON participant
FOR EACH ROW EXECUTE FUNCTION fcCheckPersonHasOnlyOneRole();

CREATE TRIGGER trCheckJuryHasOnlyOneRole AFTER INSERT OR UPDATE ON jury
FOR EACH ROW EXECUTE FUNCTION fcCheckPersonHasOnlyOneRole();

CREATE TRIGGER trCheckManagerHasOnlyOneRole AFTER INSERT OR UPDATE ON manager
FOR EACH ROW EXECUTE FUNCTION fcCheckPersonHasOnlyOneRole();

-- Vérifie le compteur lors de l'accecptation d'un membre dans une equipe
CREATE OR REPLACE FUNCTION fcCheckCounter() RETURNS TRIGGER AS $fcCheckCounter$
BEGIN
  IF OLD.counter <> NEW.counter THEN
    RAISE EXCEPTION 'Versions are different';
  END IF;
  
  NEW.counter := NEW.counter + 1;
  
  RETURN NEW;
END;
$fcCheckCounter$  LANGUAGE plpgsql;

CREATE TRIGGER trfcCheckCounter
BEFORE UPDATE ON teamComposition
FOR EACH ROW EXECUTE FUNCTION fcCheckCounter();

CREATE TRIGGER trfcCheckCounter
BEFORE UPDATE ON vote
FOR EACH ROW EXECUTE FUNCTION fcCheckCounter();

CREATE TRIGGER trfcCheckCounter
BEFORE UPDATE ON contest
FOR EACH ROW EXECUTE FUNCTION fcCheckCounter();


-- C6 Check that contest is not modified after the date of register

CREATE OR REPLACE FUNCTION fcCheckContestModification() RETURNS TRIGGER AS $fcCheckContestModification$
DECLARE
	bReg TIMESTAMP;
BEGIN
	IF OLD.beginRegister < NOW() THEN
		RAISE EXCEPTION 'You can not modify a Contest after its register time has begun';
	END IF;
    RETURN NEW;
END;
$fcCheckContestModification$ LANGUAGE plpgsql;

--CREATE TRIGGER trCheckContestModification BEFORE UPDATE ON contest
--FOR EACH ROW EXECUTE FUNCTION fcCheckContestModification();


# How to use

This is a small docker-compose for postgres. The credentials are the following:

```
- POSTGRES_USER=postgres
- POSTGRES_PASSWORD=postgres
```

## To launch
This command will launch the container in detached mode 
```
docker-compose up -d
```
When it is up you can connect to it
```
docker container exec -it NAME-YOUR-CONTAINER-HERE bash
```

## Load the DB

All the sql scripts that are stored in the sql_scripts folder will be mounted in the postgres app in the /sql_scripts folder (maybe later change its position).

Now it is time to load one like this:
```
psql -U postgres --file sql_scripts/gamejam.sql
```
You can now check if what you did was working you can connect to the db with the following command:
```
psql gamejamdb -U postgres
```

Now run SQL commands and check that what you did is working
```
SELECT * FROM gj_person;
```
